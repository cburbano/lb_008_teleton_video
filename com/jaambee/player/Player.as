﻿package com.jaambee.player {
	import fl.video.*;
	import flash.display.*;
	import flash.events.*;
	import flash.media.*;
	import flash.geom.*;
	import com.jaambee.utils.*;
	
	public class Player extends MovieClip{
		
		public var player:FLVPlayback = new FLVPlayback();
		
		private var escala:String;
		private var posicion:String;
		private var rotacion:Number;
		private var head:Head;
		private var cargador:Cargador;
		
		public function Player(head:Object) {
			
			this.head = new Head(head);
			this.head.addEventListener(Head.BOCA_LISTA, onBocaLista);
			this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event) {
			player.source = Util.URL_VIDEO;
			player.width = Util.PLAYER_WIDTH;
			player.height = Util.PLAYER_HEIGHT;
			player.align = VideoAlign.TOP_LEFT;
			player.autoPlay = false;
			player.bufferTime = Util.BUFFER_TIME;
			cargador = new Cargador();
			this.agregarCentrado(cargador);
			player.addEventListener(VideoEvent.READY, onVideoReady);
			
		}
		
		private function onBocaLista(e:Event) {
			trace("boca cargada!");
		}
		
		private function onVideoReady(e:VideoEvent) {
			Animar.fadeOut(cargador);
			container.inside.addChild(player);
			container.inside.addChild(this.head);
			playButton.buttonMode = true;
			playButton.addEventListener(MouseEvent.CLICK, onPlay);
			player.addEventListener(MetadataEvent.CUE_POINT, cuePointListener);
		}
		
		private function onPlay(e:MouseEvent) {
			playButton.visible = false;
			player.addEventListener(Event.COMPLETE, onVideoComplete);
			head.start();
			player.play();
		}
		
		private function onVideoComplete(e:Event) {
			playButton.visible = true;
		}
		
		private function cuePointListener(e:MetadataEvent) {
			var objeto:Object; 
			var prop:String;
			
			objeto = e.info;
			head.track(objeto);
		}
		
		private function agregarCentrado(objeto:DisplayObject) {
			objeto.x = this.stage.stageWidth/2;
			objeto.y = this.stage.stageHeight/2;
			addChild(objeto);
			Animar.fadeIn(objeto);
		}

	}
	
}
