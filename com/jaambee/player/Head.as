﻿package com.jaambee.player {
	import fl.video.*;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.media.*;
	import fl.video.*;
	import flash.display.Bitmap;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.display.Shape;
	import flash.display.Sprite;
	import com.jaambee.utils.*;
	
	public class Head extends MovieClip {
		
		public static const BOCA_LISTA:String = "bocaLista";
		
		private var escala:Number;
		private var posicion:Point;
		private var posicionBoca:Point;
		private var rotacion:Number;
		private var imagen:Bitmap;
		private var player:FLVPlayback = new FLVPlayback();
		
		public function Head(data:Object) {
			player.source = Util.URL_BOCA;
			player.autoPlay = false;
			player.bufferTime = Util.BUFFER_TIME;
			player.volume = 0;
			this.imagen = data.bitmap;
			this.posicionBoca = data.posicionBoca;
			
			player.x = this.posicionBoca.x - Util.CROP_X - Util.BOCA_WIDTH*1.2/2 - 280;
			player.y = this.posicionBoca.y - Util.CROP_Y - Util.BOCA_HEIGHT*1.2 + 20;
			player.width = Util.BOCA_WIDTH*1.2;
			player.height = Util.BOCA_HEIGHT*1.2;
			
			player.addEventListener(VideoEvent.READY, onBocaReady);
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event) {
		}
		
		private function onBocaReady(e:VideoEvent) {
			addChild(imagen);
			addChild(player);
			dispatchEvent(new Event(BOCA_LISTA));
		}
		
		public function start() {
			this.player.play();
		}

		public function track(dato:Object) {
			var prop:String;
			var data = dato.parameters;
			
			for (prop in data)
			{
				if(data["Transformar_Escala"]) {
					escala = data["Transformar_Escala"].toString().split(',')[0];
				
					this.height = escala/100*Util.HEIGHT_CROP;
					this.width = escala/100*Util.WIDTH_CROP;
				
				}
				
				if(data["Transformar_Posición"]) {
					posicion = new Point(Number(data["Transformar_Posición"].toString().split(',')[0]), Number(data["Transformar_Posición"].toString().split(',')[1]));
					this.x = posicion.x - this.width/2;
					this.y = posicion.y - this.height/5;
				}
				
				if(data["Transformar_Rotación"]) {
					rotacion = data["Transformar_Rotación"];
					this.rotation = rotacion - 90;
				}
			}
		}

	}
	
}
