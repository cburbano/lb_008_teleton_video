﻿package com.jaambee.views.webcam {
	
	import flash.display.MovieClip;
	import flash.media.Camera;
	import flash.media.Video;
	import flash.events.StatusEvent;
	import flash.geom.Rectangle;
	import flash.display.Sprite;
	import flash.display.Shape;
	import flash.display.DisplayObject;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	
	
	public class WebcamContainer extends MovieClip {
		
		public static const CAPTURA:String = "captura";
		
		private var camara:Camera;
		private var video:Video;
		private var mascara:Shape;
		public var foto:Sprite = new Sprite();
		
		public function WebcamContainer() {
			
		}
		
		public function iniciar() {
			
			camara = Camera.getCamera();
			camara.addEventListener(StatusEvent.STATUS, seleccionCamara);
			camara.setMode(1280,960,30);
			
			if(camara!=null) {
				mascara = new Shape();
				addChild(mascara);
				
				video = new Video(640, 480);
				video.scaleX = -1;
				video.x = -video.width/2 + video.width + video.x;
				video.y = -video.height/2;
				video.attachCamera(camara);

				addChild(video);
				
				dibujarMask(video);
			}
		}

		private function dibujarMask(video:DisplayObject):void {
			mascara.graphics.lineStyle();
			mascara.graphics.beginFill(0xFFFFFF,1);
			mascara.graphics.drawRect(0,0,400,300);
			mascara.graphics.endFill();
			
			video.mask = mascara;
			
			mascara.x = -mascara.width/2;
			mascara.y = -mascara.height/2;
		}
		
		public function capturaFoto() { 
			var snap:BitmapData = new BitmapData(video.width, video.height);
			
			video.mask = null;
            snap.draw(video);
			video.mask = mascara;
			var fotoBitmap:Bitmap = new Bitmap(snap);
			fotoBitmap.smoothing = true;
			fotoBitmap.scaleX = -1;
			fotoBitmap.x = -fotoBitmap.width/2 + fotoBitmap.width + video.x;
			foto.addChild(fotoBitmap);
			
			dispatchEvent(new Event(CAPTURA, true));

		}
		
		
		private function seleccionCamara(e:StatusEvent) {
			
		}
	}
	
}
