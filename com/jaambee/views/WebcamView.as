﻿package com.jaambee.views {
	
	import flash.display.MovieClip;
	import com.jaambee.views.webcam.WebcamContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import com.jaambee.utils.*;
	
	
	public class WebcamView extends MovieClip {
		
		public function WebcamView() {
			this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event) {
			var tomarFoto:BotonGenerico = new BotonGenerico("Tomar foto", 2);
			tomarFoto.x = Util.TOMAR_FOTO_X;
			tomarFoto.y = Util.TOMAR_FOTO_Y;
			addChild(tomarFoto);
			webcam.iniciar();
			tomarFoto.addEventListener(MouseEvent.CLICK, onTomarFoto);
		}
		
		private function onTomarFoto(e:MouseEvent) {
			webcam.capturaFoto();
		}
		
		private function onCaptura(e:Event) {
		}
	}
	
}
