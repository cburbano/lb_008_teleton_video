﻿package com.jaambee.views {
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import fl.events.*;
	
	import com.greensock.TweenMax;
	import com.greensock.easing.Expo;
	import com.jaambee.utils.Boca;
	import com.jaambee.utils.Util;
	import flash.geom.Point;
	import com.jaambee.utils.Imagen;
	import com.jaambee.utils.BotonGenerico;
	
	
	public class EdicionCara extends MovieClip {
		
		public static const LISTA_EDICION:String = "listaEdicion";
		
		private var imagen:Sprite;
		private var boca:Boca;
		private var aceptar:BotonGenerico;
		public var data:Object;
		
		public function EdicionCara(imagen:Sprite) {
			this.imagen = imagen;
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}

		private function onAdded(e:Event) {
			aceptar = new BotonGenerico("Aceptar", 1);
			aceptar.x = Util.ACEPTAR_X;
			aceptar.y = Util.ACEPTAR_Y;
			addChild(aceptar);
			aceptar.addEventListener(MouseEvent.CLICK, onBoca);
			this.agregarCentrado(contenedor.inside, this.imagen);
			
			controles.escala.addEventListener(SliderEvent.CHANGE, escalar);
			controles.rotacion.addEventListener(SliderEvent.CHANGE, rotar);
			controles.escala.getChildAt(1).width =  20;
			controles.escala.getChildAt(1).height =  20;
			controles.rotacion.getChildAt(1).width = 20;
			controles.rotacion.getChildAt(1).height = 20;
			
			contenedor.inside.addEventListener(MouseEvent.MOUSE_DOWN, dragOn);
			contenedor.inside.addEventListener(MouseEvent.MOUSE_UP, dragOff);
		}
		
		private function onBoca(e:MouseEvent) {
			aceptar.removeEventListener(MouseEvent.CLICK, onBoca);
			controles.escala.removeEventListener(SliderEvent.CHANGE, escalar);
			controles.rotacion.removeEventListener(SliderEvent.CHANGE, rotar);
			contenedor.inside.removeEventListener(MouseEvent.MOUSE_DOWN, dragOn);
			contenedor.inside.removeEventListener(MouseEvent.MOUSE_UP, dragOff);
			
			TweenMax.to(textoModificar, .7, {x:-textoModificar.width-50, ease:Expo.easeOut});
			TweenMax.to(textoSonrisa, .7, {x:0, ease:Expo.easeOut});
			boca = new Boca();
			boca.name = "boca";
			Util.posicionar(boca);
			boca.buttonMode = true;
			boca.addEventListener(MouseEvent.MOUSE_DOWN, dragOn);
			boca.addEventListener(MouseEvent.MOUSE_UP, dragOff);
			addChild(boca);
			aceptar.addEventListener(MouseEvent.CLICK, onAceptar);
		}
		
		private function onAceptar(e:MouseEvent) {
			var envio:Object = new Object();
			envio.posicionBoca = new Point(boca.x, boca.y);
			aceptar.removeEventListener(MouseEvent.CLICK, onAceptar);
			
			var bitmap:Bitmap = Imagen.crop(Util.CROP_X, Util.CROP_Y, Util.WIDTH_CROP, Util.HEIGHT_CROP, contenedor);
			envio.bitmap = bitmap;
			this.data = envio;
			dispatchEvent(new Event(LISTA_EDICION, true));
		}
		
		private function dragOn(e:Event) {
			e.currentTarget.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP, dragOff)
		}
		
		private function dragOff(e:Event) {
			e.target.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP, dragOff)
		}
		
		private function escalar(e:Event):void{
			TweenMax.to(contenedor.inside, .45, {scaleX:e.target.value/100, scaleY:e.target.value/100, ease:Expo.easeOut})
		}
		
		private function rotar(e:Event):void{
			TweenMax.to(contenedor.inside, .45, {rotation:e.target.value-180, ease:Expo.easeOut})
		}
		
		private function agregarCentrado(padre:MovieClip, objeto:DisplayObject) {
			objeto.x = -objeto.width/2;
			objeto.y = -objeto.height/2;
			padre.addChild(objeto);
		}
	}
	
}
