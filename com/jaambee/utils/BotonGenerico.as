﻿package com.jaambee.utils {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	public class BotonGenerico extends MovieClip {
		
		
		public function BotonGenerico(label:String, tipo:Number) {
			this.buttonMode = true;
			this.cuerpo.texto.text = label;
			circulo.gotoAndStop(tipo);
			this.addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		private function onAdded(e:Event) {
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}

		private function onMouseOver(e:MouseEvent) {
			Animar.overBoton(this.circulo, 1);
		}
		
		private function onMouseOut(e:MouseEvent) {
			Animar.outBoton(this.circulo, 1);
		}
	}
	
}
