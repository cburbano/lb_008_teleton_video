﻿package com.jaambee.utils {
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.EventDispatcher;
	import flash.display.DisplayObject;
	import flash.display.BitmapData;
	import com.adobe.images.JPGEncoder;
	import flash.utils.ByteArray;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	public class DataService extends EventDispatcher {
		
		private static const URL_ENVIO:String = "url de envio";
		
		public function DataService() {
			
		}

		public function cargarImagen(url:String) {
			var loader:Loader = new Loader();
			var req:URLRequest = new URLRequest(url);
			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteImagen);
			loader.load(req);
		}
		
		private function onCompleteImagen(e:Event) {
			var imagen:Sprite = new Sprite();
			imagen.addChild(e.target.content);
			dispatchEvent(new DataServiceEvent(DataServiceEvent.IMAGEN_CARGADA, imagen));
		}
		
		public function guardarImagen(imagen:BitmapData) {
			var encoder:JPGEncoder = new JPGEncoder(50);
			var data:ByteArray = encoder.encode(imagen);
			
			var header:URLRequestHeader = new URLRequestHeader("Content-type","application/octet-stream");
			var req:URLRequest = new URLRequest(URL_ENVIO);
			req.requestHeaders.push(header);
			req.method = URLRequestMethod.POST;
		   
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onEnvioCompletado);
		}
		
		private function onEnvioCompletado(e:Event) {
			var dataStr:String = e.currentTarget.data.toString();
			var resultVars:URLVariables = new URLVariables();
			resultVars.decode(dataStr);
			dispatchEvent(new DataServiceEvent(DataServiceEvent.IMAGEN_GUARDADA, resultVars));
		}

	}
	
}
