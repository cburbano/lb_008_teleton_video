﻿package com.jaambee.utils {
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.geom.Matrix;
	
	
	public class Imagen {

		public function Imagen() {
			
		}

		public static function crop( _x:Number, _y:Number, _width:Number, _height:Number, displayObject:DisplayObject = null):Bitmap
		{
		   var cropArea:Rectangle = new Rectangle( 0, 0, _width, _height );
		   var croppedBitmap:Bitmap = new Bitmap( new BitmapData( _width, _height ), PixelSnapping.ALWAYS, true );
		   croppedBitmap.bitmapData.draw(displayObject, new Matrix(1, 0, 0, 1, -_x, -_y) , null, null, cropArea, true );
		   return croppedBitmap;
		}
	}
	
}
