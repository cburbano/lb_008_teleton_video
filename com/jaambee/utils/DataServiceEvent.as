﻿package com.jaambee.utils {
	import flash.events.Event;
	
	public class DataServiceEvent extends Event {
		
		public static const IMAGEN_CARGADA:String = "imagenCargada";
		public static const IMAGEN_GUARDADA:String = "imagenGuardada";
		
		public var data:Object;
		
		public function DataServiceEvent(type:String, data:Object, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
			this.data = data;
		}
		
		public override function clone():Event
		{
			return new DataServiceEvent(type, data, bubbles, cancelable);
		}
	}
	
}
