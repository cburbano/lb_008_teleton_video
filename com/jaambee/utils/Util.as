﻿package com.jaambee.utils {
	import flash.display.DisplayObject;
	
	public class Util {
		
		public static const CROP_X:Number = 300 - WIDTH_CROP/2;
		public static const CROP_Y:Number = 180 - HEIGHT_CROP/2;
		
		public static const WIDTH_CROP:Number = 205;
		public static const HEIGHT_CROP:Number = 270;
		
		public static const URL_VIDEO:String = "media/video.flv";
		public static const URL_BOCA:String = "media/boca.flv";
		
		public static const PLAYER_WIDTH:Number = 700;
		public static const PLAYER_HEIGHT:Number = 368;
		public static const BUFFER_TIME:Number = 8;
		
		private static const BASE_X:Number = 30;
		private static const BASE_Y:Number = 40;
		
		private static const BOCA_X:Number = 580;
		private static const BOCA_Y:Number = 250;
		
		public static const BOCA_WIDTH:Number = 90;
		public static const BOCA_HEIGHT:Number = 40;
		
		public static const TOMAR_FOTO_X:Number = 840;
		public static const TOMAR_FOTO_Y:Number = 350;
		
		public static const ACEPTAR_X:Number = 840;
		public static const ACEPTAR_Y:Number = 350;
		
		public static const CIRCULO_X:Number = 90;

		public function Util() {
			
		}

		public static function posicionar(objeto:DisplayObject) {
			switch(objeto.name) {
				case "boca":
					objeto.x = Util.BOCA_X;
					objeto.y = Util.BOCA_Y;
				break;
				
				case "webcam":
					objeto.x = Util.BASE_X;
					objeto.y = Util.BASE_Y;
				break;
				
				case "editor":
					objeto.x = Util.BASE_X;
					objeto.y = Util.BASE_Y;
				break;
				
				case "player":
					objeto.x = Util.BASE_X;
					objeto.y = Util.BASE_Y;
				break;
			}
		}

	}
	
}
