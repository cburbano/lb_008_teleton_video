﻿package com.jaambee.utils {
	import flash.display.*;
	import com.greensock.TweenMax;
	import com.greensock.easing.*;
	
	
	public class Animar {

		public function Animar() {
			
		}

		public static function overBoton(objeto:DisplayObject, t:Number=.5, delay:Number=0) {
			TweenMax.to(objeto, t, {x:Util.CIRCULO_X+10, delay:delay, ease:Expo.easeOut});
		}
		
		public static function outBoton(objeto:DisplayObject, t:Number=.5, delay:Number=0) {
			TweenMax.to(objeto, t, {x:Util.CIRCULO_X, delay:delay, ease:Expo.easeOut});
		}
		
		public static function fadeIn(objeto:DisplayObject, t:Number=.5,delay:Number=0) {
			objeto.alpha = 0;
			objeto.visible = false;
			TweenMax.to(objeto, t, {autoAlpha:1, delay:delay});
		}
		
		public static function fadeOut(objeto:DisplayObject, kill:Boolean=false, t:Number=.5,delay:Number=0) {
			if(!kill) {	
				TweenMax.to(objeto, t, {autoAlpha:0, delay:delay});
			} else {
				TweenMax.to(objeto, t, {autoAlpha:0, delay:delay, onComplete:kill, onCompleteParams:[objeto]});
			}
		}
		
		/*revisar esta funcion porque genera el siguiente error:
		TypeError: Error #1006: value is not a function.
			at com.greensock::TweenMax/render()
			at com.greensock.core::SimpleTimeline/render()
			at com.greensock.core::Animation$/_updateRoot()
		*/
		private function kill(objeto:DisplayObject) {
			objeto.parent.removeChild(objeto);
			trace("removido!");
		}
	}
	
}
