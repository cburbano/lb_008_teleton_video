﻿package com.jaambee.utils {
	import flash.events.Event;
	
	public class ImagenEvent extends Event {
		
		public static const IMAGEN_CORTADA:String = "imagenCortada";
		
		public var data:Object;
		
		public function ImagenEvent(type:String, data:Object, bubbles:Boolean=false, cancelable:Boolean=false) {
			super(type, bubbles, cancelable);
			this.data = data;
		}
		
		public override function clone():Event
		{
			return new ImagenEvent(type, data, bubbles, cancelable);
		}
	}
	
}
