﻿package com.jaambee {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import com.jaambee.player.Player;
	import com.jaambee.util.Assets;
	
	import com.jaambee.views.WebcamView;
	import com.jaambee.views.webcam.WebcamContainer;
	import com.jaambee.views.EdicionCara;
	import com.jaambee.utils.DataService;
	import com.jaambee.utils.DataServiceEvent;
	import com.jaambee.utils.Animar;
	import com.jaambee.utils.Cargador;
	
	import com.greensock.TweenMax;
	import com.jaambee.utils.Util;
	
	
	public class App extends MovieClip {
		
		
		private var webcamFlag:Boolean;
		private var url:String;
		
		private var webcam:WebcamView = new WebcamView();
		private var editor:EdicionCara;
		private var player:Player;
		private var cargador:Cargador;
		
		private var assets:Assets = new Assets();
		
		
		public function App() {
			addChild(assets);
			if(this.getParams()) {
				webcam.name = "webcam";
				Util.posicionar(webcam);
				webcam.addEventListener(WebcamContainer.CAPTURA, onCaptura);
				addChild(webcam);	
			} else {
				var cargaImagen:DataService = new DataService();
				cargaImagen.cargarImagen("http://localhost/teleton/imagen2.jpg");
				
				cargador = new Cargador();
				this.agregarCentrado(cargador);
				
				cargaImagen.addEventListener(DataServiceEvent.IMAGEN_CARGADA, onImagenCargada);
			}
		}
		
		private function getParams():Boolean {
			webcamFlag = root.loaderInfo.parameters.webcam as Boolean;
			if(!webcamFlag)
				this.url = root.loaderInfo.parameters.url as String;
			return true;
		}
		
		private function onImagenCargada(e:DataServiceEvent) {
			
			Animar.fadeOut(cargador, false, 1);
			
			editor = new EdicionCara(e.data as Sprite);
			editor.name = "editor";
			Util.posicionar(editor);
			editor.addEventListener(EdicionCara.LISTA_EDICION, onEdicion);
			addChild(editor);
			Animar.fadeIn(editor, 1);
		}
		
		private function onCaptura(e:Event) {
			editor = new EdicionCara(e.target.foto);
			editor.name = "editor";
			Util.posicionar(editor);
			editor.addEventListener(EdicionCara.LISTA_EDICION, onEdicion);
			addChild(editor);
			Animar.fadeIn(editor, 1);
			Animar.fadeOut(webcam, false, 1);
		}
		
		private function onEdicion(e:Event) {
			Animar.fadeOut(editor, false, 1);
			player = new Player(editor.data);
			player.name = "player";
			Util.posicionar(player);
			addChild(player);
			Animar.fadeIn(player, 1);
		}
		
		private function agregarCentrado(objeto:DisplayObject) {
			objeto.x = this.stage.stageWidth/2;
			objeto.y = this.stage.stageHeight/2;
			addChild(objeto);
			Animar.fadeIn(objeto, 1);
		}
	}
	
}
