﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class eligeCara extends MovieClip{
		var caraUno:Personaje = new Personaje();
		var caraDos:Personaje = new Personaje();
		
		var prop1:Number;
		var prop2:Number;
		var videos:String;
		
		var height1a:Number;
		var height2a:Number;
		
		var height1b:Number;
		var height2b:Number;
		
		function eligeCara():void{
			
			height1a= practicante.cara1.inUno.height;
			height2a= practicante.cara2.inDos.height;
			height1b= clase.cara1.inUno.height;
			height2b= clase.cara2.inDos.height;
			
			prevBtn.visible = false;
			prevBtn.alpha = 0;
			eligeUnoBtn.addEventListener(MouseEvent.CLICK, eligeUno);
			eligeDosBtn.addEventListener(MouseEvent.CLICK, eligeDos);
			prevBtn.addEventListener(MouseEvent.CLICK, prevVideo);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
			atrasBtn.addEventListener(MouseEvent.CLICK, atrasFunc);
		}
		
		function revisaCabezas() {
			if(videos == "practicante") {
				clase.visible = false;
				practicante.visible = true;
			if(caraUno.creada) {
				prop1 = height1a/caraUno.cabeza.height;
				practicante.cara1.inUno.visible = false;
				if(practicante.cara1.numChildren ==3)
					practicante.cara1.removeChildAt(2);
				practicante.cara1.nombreTxt.text = caraUno.nombre;
				caraUno.cabeza.scaleY = caraUno.cabeza.scaleX = prop1;
				caraUno.boca.scaleY = caraUno.boca.scaleX = prop1;
				caraUno.creada = false;
				caraUno.checka = true;
				practicante.cara1.addChild(caraUno.cabeza);
			}
			if(caraDos.creada) {
				prop2 = height2a/caraDos.cabeza.height;
				practicante.cara2.inDos.visible = false;
				if(practicante.cara2.numChildren ==3)
					practicante.cara2.removeChildAt(2);
				practicante.cara2.nombreTxt.text = caraDos.nombre;
				caraDos.cabeza.scaleY = caraDos.cabeza.scaleX = prop2;
				caraDos.boca.scaleY = caraDos.boca.scaleX = prop2;
				caraDos.creada = false;
				caraDos.checka = true;
				practicante.cara2.addChild(caraDos.cabeza);
			}
			}
			else {
				clase.visible = true;
				practicante.visible = false;
			if(caraUno.creada) {
				if(clase.cara1.numChildren ==3)
					clase.cara1.removeChildAt(2);
				prop1 = clase.cara1.inUno.height/caraUno.cabeza.height;
				clase.cara1.inUno.visible = false;
				clase.cara1.nombreTxt.text = caraUno.nombre;
				caraUno.cabeza.scaleY = caraUno.cabeza.scaleX = prop1;
				caraUno.boca.scaleY = caraUno.boca.scaleX = prop1;
				caraUno.creada = false;
				caraUno.checka = true;
				clase.cara1.addChild(caraUno.cabeza);
			}
			if(caraDos.creada) {
				if(clase.cara2.numChildren ==3)
					clase.cara2.removeChildAt(2);
				prop2 = clase.cara2.inDos.height/caraDos.cabeza.height;
				clase.cara2.inDos.visible = false;				
				clase.cara2.nombreTxt.text = caraDos.nombre;
				caraDos.cabeza.scaleY = caraDos.cabeza.scaleX = prop2;
				caraDos.boca.scaleY = caraDos.boca.scaleX = prop2;
				caraDos.creada = false;
				caraDos.checka = true;
				clase.cara2.addChild(caraDos.cabeza);
			}
			}
			if(revisaCaras() == 2) {
				prevBtn.visible = true;
				TweenMax.to(prevBtn, .75, {autoAlpha:1, ease:Expo.easeOut});
		
			}
		}
				
		function revisaCaras():Number{
			var total:Number=0;
			
			if(caraUno.checka)
				total++;
			if(caraDos.checka)
				total++;
			return total;
		}
		
		function prevVideo(e:MouseEvent) {
			quitaMovie(2,5);
		}
		
		function eligeUno(e:MouseEvent):void {
			quitaMovie(1,2);		
		}
		
		function eligeDos(e:MouseEvent):void {
			quitaMovie(2,2);		
		}
		
		function atrasFunc(e:MouseEvent) {
			 caraUno = new Personaje();
			 caraDos = new Personaje();
			quitaMovie(0, 0);
		}
		
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
		}
		
		function init() {
			prevBtn.visible = false;
			prevBtn.alpha = 0;
			if(videos == "practicante") {
				clase.visible = false;
				practicante.visible = true;
			} else {
				clase.visible = true;
				practicante.visible = false;
			}
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut});
		}
		
		function quitaMovie(personaje:Number, modo:Number):void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente, onCompleteParams:[personaje, modo]});
		}
		
		function initSiguiente(personaje:Number, modo:Number):void {
			var padre = MovieClip(parent);
			visible = false;
			if(modo == 0)
				padre.eligeVideo.init();
			if(modo == 5)
			{
				padre.prevVideo.init();
			}
			else if((modo >0) && (modo <5))
			{
				padre.seleccionaImagen.init();
				padre.editarCara.pNumero = personaje;
				padre.editarCara.modo = modo;
			}
		} 
	}
}