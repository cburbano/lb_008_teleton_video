﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class menuLateral extends MovieClip{
	
		
		function menuLateral():void{
			initForms();
			creaVideoBtn.addEventListener(MouseEvent.CLICK, initCrear);
			
		}
		
		function initForms():void{
			eligeVideo.visible =confirmacionMc.visible  = elige4Caras.visible = eligeCara.visible = seleccionaImagen.visible = bgLateral.visible = editarCara.visible = prevVideo.visible = enviarAmigo.visible = webcamFoto.visible = false;
			eligeVideo.alpha = confirmacionMc.alpha = elige4Caras.alpha = eligeCara.alpha = seleccionaImagen.alpha = bgLateral.alpha = editarCara.alpha = prevVideo.alpha = enviarAmigo.alpha = webcamFoto.alpha = 0;
		}
		
		function closeAll():void{
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			/*prevVideo.yaseHizo = false;
			
			TweenMax.to(eligeVideo, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(elige4Caras, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(eligeCara, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(seleccionaImagen, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(bgLateral, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(editarCara, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(prevVideo, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(enviarAmigo, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(webcamFoto, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			TweenMax.to(confirmacionMc, .75, {autoAlpha:0, ease:Expo.easeOut, onComplete:onCloseAll})
			
			eligeCara.videos = "";
			
			if(eligeCara.practicante.cara1.numChildren ==3)
					eligeCara.practicante.cara1.removeChildAt(2);
			if(eligeCara.practicante.cara2.numChildren ==3)
					eligeCara.practicante.cara2.removeChildAt(2);
			if(eligeCara.clase.cara1.numChildren ==3)
					eligeCara.clase.cara1.removeChildAt(2);
			if(eligeCara.clase.cara2.numChildren ==3)
					eligeCara.clase.cara2.removeChildAt(2);
					
			if(elige4Caras.cara1.numChildren ==3)
				elige4Caras.cara1.removeChildAt(2);
			if(elige4Caras.cara2.numChildren ==3)
				elige4Caras.cara2.removeChildAt(2);
			if(elige4Caras.cara3.numChildren ==3)
				elige4Caras.cara3.removeChildAt(2);
			if(elige4Caras.cara4.numChildren ==3)
				elige4Caras.cara4.removeChildAt(2);
					
			
			eligeCara.clase.cara1.inUno.visible = true;
			eligeCara.clase.cara2.inDos.visible = true;
			eligeCara.caraUno.creada = false;
			eligeCara.caraDos.creada = false;
			eligeCara.caraUno.checka = false;
			eligeCara.caraDos.checka = false;
			eligeCara.clase.cara1.scaleX = eligeCara.clase.cara1.scaleX = 1;
			eligeCara.clase.cara2.scaleX = eligeCara.clase.cara2.scaleX = 1;
			eligeCara.clase.cara1.nombreTxt.text = "";
			eligeCara.clase.cara2.nombreTxt.text = "";
						
			elige4Caras.cara1.inUno.visible = true;
			elige4Caras.cara2.inDos.visible = true;
			elige4Caras.cara3.inTres.visible = true;
			elige4Caras.cara4.inCuatro.visible = true;
			
			creaVideoBtn.addEventListener(MouseEvent.CLICK, initCrear);		
			statusMc.posFlecha(1);
			creaVideoBtn.enabled = true;*/
		}
		
		function onCloseAll():void {
			//eligeVideo.visible = elige4Caras.visible = eligeCara.visible = confirmacionMc.visible = seleccionaImagen.visible = bgLateral.visible = editarCara.visible = prevVideo.visible = enviarAmigo.visible = webcamFoto.visible = false;
			MovieClip(parent).cierraFuncion();
		}
		function initCrear(e:MouseEvent):void{
			creaVideoBtn.removeEventListener(MouseEvent.CLICK, initCrear);
			creaVideoBtn.enabled = false;
			bgLateral.visible = true;
			eligeVideo.init();
			TweenMax.to(bgLateral, 1, {autoAlpha:1, ease:Expo.easeOut});
			statusMc.posFlecha(2);
			statusMc.flechaStatus.gotoAndStop(1);
			
		}
		
		
		
	}
}