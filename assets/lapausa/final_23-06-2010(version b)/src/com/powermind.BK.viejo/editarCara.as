﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import fl.events.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class editarCara extends MovieClip{
		var pNumero:Number;
		var modo:Number;
		var errorBox:ErrorBox;
		var cabeza:MovieClip;
		var boca:MovieClip;
			
		function editarCara():void{
			mascaraEditar.loaderImagen.addEventListener(MouseEvent.MOUSE_DOWN, dragOn);
			mascaraEditar.loaderImagen.addEventListener(MouseEvent.MOUSE_UP, dragOff);
			mascaraEditar.loaderImagen.mask = mascaraEditar.mascara.maskMc;
			ahoraMc.visible = false;
			ahora0Mc.visible = true;
			maskBoca.loaderImagen.mask = null;
			if(maskBoca.loaderImagen.loadImg.numChildren > 0)
			maskBoca.loaderImagen.loadImg.removeChildAt(0);
			
			escalarMc.escalar_slider.addEventListener(SliderEvent.CHANGE, escalar);
			rotarMc.rotar_slider.addEventListener(SliderEvent.CHANGE, rotar);
			
			aceptarBtn.visible = false;
			maskBoca.visible = false;
			siguienteBtn.addEventListener(MouseEvent.CLICK, siguienteFunc);
			aceptarBtn.addEventListener(MouseEvent.CLICK, aceptarFunc);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
		}
		
		function siguienteFunc(e:MouseEvent) {
			
			this.cabeza = cortarEv(pNumero);
			mascaraEditar.alpha = .5;
			maskBoca.visible = true;
			aceptarBtn.visible = true;
			siguienteBtn.visible = false;
			mascaraEditar.loaderImagen.removeEventListener(MouseEvent.MOUSE_DOWN, dragOn);
			mascaraEditar.loaderImagen.removeEventListener(MouseEvent.MOUSE_UP, dragOff);
			ahoraMc.visible = true;
			ahora0Mc.visible = false;
			escalarMc.visible = false;
			rotarMc.visible = false;
		}
		
		function aceptarFunc(e:MouseEvent) {
			
			maskBoca.loaderImagen.mask = maskBoca.maskara.maskMc;
			maskBoca.loaderImagen.loadImg.addChild(this.cabeza);
			var padre = MovieClip(parent);
			if(nombreMc.nombreTxt.text != "") {
			this.boca = creaBoca(cortarBoca());
			quitaMovie();
			if(modo == 4) {
			switch(pNumero) {
				case 1:
				padre.elige4Caras.caraUno.cabeza = this.cabeza;
				padre.elige4Caras.caraUno.boca = this.boca;
				padre.elige4Caras.caraUno.bmdb = cortarBoca();
				padre.elige4Caras.caraUno.nombre = nombreMc.nombreTxt.text;
				padre.elige4Caras.caraUno.numero = 1;
				padre.elige4Caras.caraUno.creada = true;
				break;
				
				case 2:
				padre.elige4Caras.caraDos.cabeza = this.cabeza;
				padre.elige4Caras.caraDos.boca = this.boca;
				padre.elige4Caras.caraDos.bmdb = cortarBoca();
				padre.elige4Caras.caraDos.nombre = nombreMc.nombreTxt.text;
				padre.elige4Caras.caraDos.numero = 2;
				padre.elige4Caras.caraDos.creada = true;
				padre.elige4Caras.caraCinco.cabeza = cortarEv(2);
				padre.elige4Caras.caraCinco.boca = creaBoca(cortarBoca());
				padre.elige4Caras.caraCinco.bmdb = cortarBoca();
				padre.elige4Caras.caraCinco.nombre = nombreMc.nombreTxt.text;
				padre.elige4Caras.caraCinco.numero = 2;
				padre.elige4Caras.caraCinco.creada = true;
				break;
				
				case 3:
				padre.elige4Caras.caraTres.cabeza = this.cabeza;
				padre.elige4Caras.caraTres.boca = this.boca;
				padre.elige4Caras.caraTres.bmdb = cortarBoca();
				padre.elige4Caras.caraTres.nombre = nombreMc.nombreTxt.text;
				padre.elige4Caras.caraTres.numero = 3;
				padre.elige4Caras.caraTres.creada = true;
				break;
				
				case 4:
				padre.elige4Caras.caraCuatro.cabeza = this.cabeza;
				padre.elige4Caras.caraCuatro.boca = this.boca;
				padre.elige4Caras.caraCuatro.bmdb = cortarBoca();
				padre.elige4Caras.caraCuatro.nombre = nombreMc.nombreTxt.text;
				padre.elige4Caras.caraCuatro.numero = 4;
				padre.elige4Caras.caraCuatro.creada = true;
				break;
			}
			}
			else {
			switch(pNumero) {
				case 1:
				padre.eligeCara.caraUno.cabeza = this.cabeza;
				padre.eligeCara.caraUno.boca = this.boca;
				padre.eligeCara.caraUno.bmdb = cortarBoca();
				padre.eligeCara.caraUno.nombre = nombreMc.nombreTxt.text;
				padre.eligeCara.caraUno.numero = 1;
				padre.eligeCara.caraUno.creada = true;
				break;
				
				case 2:
				padre.eligeCara.caraDos.cabeza = this.cabeza;
				padre.eligeCara.caraDos.boca = this.boca;
				padre.eligeCara.caraDos.bmdb = cortarBoca();
				padre.eligeCara.caraDos.nombre = nombreMc.nombreTxt.text;
				padre.eligeCara.caraDos.numero = 2;
				padre.eligeCara.caraDos.creada = true;
				break;
				}
			}
			}
			else {
				errorBox = new ErrorBox("Error en el Nombre", "Debe ingresar un nombre. Intente nuevamente.");
				addChild(errorBox);
			}
			
		}
		function cierraFunc(e:MouseEvent) {
			quitaMovie();
		}
		
		function cortarBoca():BitmapData {
			var bounds:Rectangle = getRealBounds(mascaraEditar);
			maskBoca.maskara.dontShow();
			var bmpd:BitmapData = new BitmapData(bounds.width, bounds.height, true, 0xFFFFFF);
			var matrix:Matrix = new Matrix();
			matrix.translate(-bounds.x, -bounds.y);
			bmpd.draw(maskBoca, matrix, null, null, null, true);
			/*var bmp:Bitmap = new Bitmap(bmpd);
			bmp.x = -bmp.width/2;
			bmp.y = -bmp.height/2;
			bmp.smoothing = true;
			mc.addChild(bmp);
			return mc;
			*/
			return bmpd;
			
			}
		function creaBoca(b:BitmapData):MovieClip {
			var mc:MovieClip = new MovieClip;
			var bmp:Bitmap = new Bitmap(b);
			bmp.x = -bmp.width/2;
			bmp.y = -bmp.height/2;
			bmp.smoothing = true;
			mc.addChild(bmp);
			return mc;
		}
		
		function cortarEv(i:Number):MovieClip {
			var padre = MovieClip(parent);
			var mc:MovieClip = new MovieClip();
			var bounds:Rectangle = getRealBounds(mascaraEditar);
			mascaraEditar.mascara.dontShow();
			var bmpd:BitmapData = new BitmapData(bounds.width, bounds.height, true, 0xFFFFFF);
			var matrix:Matrix = new Matrix();
			matrix.translate(-bounds.x, -bounds.y);
			bmpd.draw(mascaraEditar, matrix, null, null, null, true);
			if(modo == 4) {
			switch(i) {
				case 1:
				padre.elige4Caras.caraUno.bmd = bmpd;
				break;
				
				case 2:
				padre.elige4Caras.caraDos.bmd = bmpd;
				padre.elige4Caras.caraCinco.bmd = bmpd;
				break;
				
				case 3:
				padre.elige4Caras.caraTres.bmd = bmpd;
				break;
				
				case 4:
				padre.elige4Caras.caraCuatro.bmd = bmpd;
				break;
			}
			} else {
			switch(i) {
				case 1:
				padre.eligeCara.caraUno.bmd = bmpd;
				break;
				
				case 2:
				padre.eligeCara.caraDos.bmd = bmpd;
				break;
			}
			}
			var bmp:Bitmap = new Bitmap(bmpd);
			bmp.x = -bmp.width/2;
			bmp.y = -bmp.height/2;
			bmp.smoothing = true;
			mc.addChild(bmp);
			return mc;
		}
		
		function getRealBounds(displayObject:DisplayObject):Rectangle
		{
			var bounds:Rectangle;
			var boundsDispO:Rectangle = displayObject.getBounds(displayObject);
			
			var bitmapData:BitmapData = new BitmapData(int(boundsDispO.width + 0.5), int(boundsDispO.height + 0.5), true, 0);
			
			var matrix:Matrix = new Matrix();
			matrix.translate(-boundsDispO.x, -boundsDispO.y);
			
			bitmapData.draw(displayObject, matrix, new ColorTransform(1, 1, 1, 1, 255, -255, -255, 255 ));
			
			bounds = bitmapData.getColorBoundsRect(0xFF000000, 0xFF000000);
			bounds.x += boundsDispO.x;
			bounds.y += boundsDispO.y;
			
			bitmapData.dispose();
			return bounds;
		}
		
		function escalar(e:Event):void{
			TweenMax.to(mascaraEditar.loaderImagen, .45, {scaleX:e.target.value/100, scaleY:e.target.value/100, ease:Expo.easeOut})
		}
		
		function rotar(e:Event):void{
			TweenMax.to(mascaraEditar.loaderImagen, .45, {rotation:e.target.value-180, ease:Expo.easeOut})
		}
		
		function dragOn(e:Event) {
			e.target.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP, dragOff)
		}
		
		function dragOff(e:Event) {
			mascaraEditar.loaderImagen.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP, dragOff)
		}
		function reset() {
			
			mascaraEditar.loaderImagen.addEventListener(MouseEvent.MOUSE_DOWN, dragOn);
			mascaraEditar.loaderImagen.addEventListener(MouseEvent.MOUSE_UP, dragOff);
			mascaraEditar.mascara.Show();
			mascaraEditar.alpha = 1;
			maskBoca.maskara.Show();
			maskBoca.visible = false;
			siguienteBtn.visible = true;
			aceptarBtn.visible = false;
			if(maskBoca.loaderImagen.loadImg.numChildren == 1)
				maskBoca.loaderImagen.loadImg.removeChildAt(0);
			if(mascaraEditar.loaderImagen.loadImg.numChildren == 1)
				mascaraEditar.loaderImagen.loadImg.removeChildAt(0);
			mascaraEditar.loaderImagen.scaleX=mascaraEditar.loaderImagen.scaleY = 1;
			mascaraEditar.loaderImagen.rotation=0;
			mascaraEditar.loaderImagen.loadImg.x = mascaraEditar.loaderImagen.loadImg.y = 0;
			escalarMc.escalar_slider.value = 100;
			rotarMc.rotar_slider.value = 180;
			nombreMc.nombreTxt.text = "";
			ahoraMc.visible = false;
			escalarMc.visible = true;
			rotarMc.visible = true;
			ahoraMc.visible = false;
			ahora0Mc.visible = true;
		}
		
		function init() {
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut});
		}
		function quitaMovie():void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente});
		}
		
		function initSiguiente():void {
			var padre = MovieClip(parent);
			reset();
			visible = false;
			if(modo == 4) {
			padre.elige4Caras.init();
			padre.elige4Caras.revisaCabezas();
			}
			else {
			padre.eligeCara.init();			
			padre.eligeCara.revisaCabezas();
			}
		} 
	}
}