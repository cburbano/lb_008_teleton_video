﻿package com.powermind {
	
	import fl.video.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.net.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	public class Reproductor extends MovieClip {
		
		private static const IMAGEN_CARGADA:String = 'imagenCargada';
		private static const BOCA_CARGADA:String = 'bocaCargada';
		private var cantidad:int;
		private var contador:int =0;
		private var contadorB:int =0;
		var tipoVideo:Number;
		
		var cargo:Boolean=false;		
		var personas:Array = new Array();
		var bocas:Array = new Array();
		var nombres:Array = new Array();
		
		var rutaBase:String = "/";
		var video:video5Track;
		var videoc:video2Trackc;
		var videop:video2Trackp;
		var b1:MovieClip = new MovieClip();
		var b2:MovieClip = new MovieClip();
		var b3:MovieClip = new MovieClip();
		var b4:MovieClip = new MovieClip();
		var b5:MovieClip = new MovieClip();
		
		var c1:MovieClip = new MovieClip();
		var c2:MovieClip = new MovieClip();
		var c3:MovieClip = new MovieClip();
		var c4:MovieClip = new MovieClip();
		var c5:MovieClip = new MovieClip();
		
		var n1:String;
		var n2:String;
		var n3:String;
		var n4:String;
		
		public function Reproductor() {
			var param:Object = LoaderInfo(this.root.loaderInfo).parameters;
			var xmlRuta:String = param["hash"];
			cargaXML(xmlRuta);
			verBtn.addEventListener(MouseEvent.CLICK, verNuevo);
			creaBtn.addEventListener(MouseEvent.CLICK, crearNuevo);
		}
		
		function verNuevo(e:MouseEvent) {
			if(video)
			video.playVideo();
			else if(videoc)
			videoc.playVideo();
			else if(videop)
			videop.playVideo();
		}
		
		function crearNuevo(e:MouseEvent) {
			navigateToURL(new URLRequest("/"));
		}
		
		function cargaVideo(i:Number) {
		switch(i) {
			case 1:
			videoc = new video2Trackc(rutaBase+"flv/clase.flv", personas[0], personas[1], bocas[0], bocas[1], nombres[0], nombres[1]);
			addChild(videoc);
			videoc.x = 220;
			videoc.y = this.stage.stageHeight/2 - videoc.height/2+100;
			videoc.playVideo();
			break;
			
			case 2:
			video = new video5Track(rutaBase+"flv/balance.flv", personas[0], personas[1], personas[2], personas[3], personas[4], bocas[0], bocas[1], bocas[2], bocas[3], nombres[0], nombres[1], nombres[2], nombres[3])
			addChild(video);
			video.x = 220;
			video.y = this.stage.stageHeight/2 - video.height/2+100;
			video.playVideo();
			break;
			
			case 3:
			videop = new video2Trackp(rutaBase+"flv/practicante.flv", personas[0], personas[1], bocas[0], bocas[1], nombres[0], nombres[1]);
			addChild(videop);
			videop.x = 220;
			videop.y = this.stage.stageHeight/2 - videop.height/2+100;
			videop.playVideo();
			break;
		}
		}
		
		function cargaXML(direccion:String) {
			
			
			var carasLoader:URLLoader = new URLLoader();
			carasLoader.addEventListener(Event.COMPLETE, showCaras);
			carasLoader.load(new URLRequest(rutaBase+"videos/getXml/"+direccion));
		}
		
		function showCaras(e:Event):void {
			personas.push(c1);
			personas.push(c2);
			personas.push(c3);
			personas.push(c4);
			personas.push(c5);
			
			bocas.push(b1);
			bocas.push(b2);
			bocas.push(b3);
			bocas.push(b4);
			bocas.push(b5);
			
			XML.ignoreWhitespace = true; 
			var i:Number=0;
			var personajes:XML = new XML(e.target.data);
			
			this.cantidad = personajes.personas.persona.length();
			tipoVideo = personajes.tipo_video;
			
			for each(var personaje:XML in personajes.personas.persona){
				this.addEventListener(IMAGEN_CARGADA, imagenCargada);
				this.addEventListener(BOCA_CARGADA, bocaCargada);
				CargarFoto(personaje.numero_cara-1, rutaBase+personaje.cara);
				CargarBoca(personaje.numero_cara-1, rutaBase+personaje.boca);
				nombres[personaje.numero_cara-1] = personaje.nombre;
			}
		}
		
		private function imagenCargada(e:Event) {
			this.contador++;
			if(this.contador == this.cantidad)
				if(this.contadorB == this.cantidad)
				cargaVideo(tipoVideo);
		}		
		
		private function bocaCargada(e:Event) {
			this.contadorB++;
			if((this.contadorB == this.cantidad))
				if(this.contador == this.cantidad)
				cargaVideo(tipoVideo);
			
		}
		
		public function CargarBoca(k:Number, url:String) {
			var loaders:Loader = new Loader();
			var peticion:URLRequest = new URLRequest(url);
			
			loaders.contentLoaderInfo.addEventListener(Event.COMPLETE, cargasImagen);
			loaders.load(peticion);
			function cargasImagen(e:Event):void {
				e.target.content.x = -e.target.content.width/2;
				e.target.content.y = -e.target.content.height;
				bocas[k].addChild(e.target.content);
				
				dispatchEvent(new Event(BOCA_CARGADA));
			}			
		}
		
		public function CargarFoto(i:Number, url:String) {
			var loader:Loader = new Loader();
			var peticion:URLRequest = new URLRequest(url);
			
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, cargaImagen);
			loader.load(peticion);
			
			function cargaImagen(e:Event):void {
				e.target.content.x = -e.target.content.width/2;
				e.target.content.y = -e.target.content.height;
				personas[i].addChild(e.target.content);
				
				dispatchEvent(new Event(IMAGEN_CARGADA));
			}			
		}

	}
}