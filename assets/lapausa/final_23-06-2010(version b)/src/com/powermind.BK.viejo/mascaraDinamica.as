﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	public class mascaraDinamica extends MovieClip {
		
		public var maskMc:MovieClip = new MovieClip();
		public var bordeMc:MovieClip = new MovieClip();
		
	public function mascaraDinamica() {
		
		setMascaraForm();
		p1.buttonMode = p2.buttonMode = p3.buttonMode = p4.buttonMode = p5.buttonMode = p6.buttonMode = p7.buttonMode = p8.buttonMode = a1.buttonMode = a2.buttonMode = a3.buttonMode = a4.buttonMode = a5.buttonMode = a6.buttonMode = a7.buttonMode = a8.buttonMode = true;
		
		p1.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p1.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p2.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p2.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p3.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p3.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p4.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p4.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p5.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p5.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p6.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p6.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p7.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p7.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p8.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p8.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a1.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a1.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a2.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a2.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a3.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a3.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a4.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a4.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a5.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a5.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a6.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a6.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a7.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a7.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a8.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a8.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
	
		addChildAt(maskMc,0);
		addChildAt(bordeMc,1);
	
		dibujaMask();
		dibujaBorde();
			
	}
	
	function poneListeners() {
		p1.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p1.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p2.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p2.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p3.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p3.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p4.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p4.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p5.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p5.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p6.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p6.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p7.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p7.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p8.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p8.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a1.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a1.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a2.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a2.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a3.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a3.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a4.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a4.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a5.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a5.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a6.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a6.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a7.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a7.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a8.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a8.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
	}
	
	
	function quitaListeners() {
		p1.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p1.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p2.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p2.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p3.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p3.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p4.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p4.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p5.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p5.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p6.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p6.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p7.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p7.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p8.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p8.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a1.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a1.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a2.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a2.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a3.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a3.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a4.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a4.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a5.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a5.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a6.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a6.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a7.removeEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a7.removeEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a8.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a8.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
	}
	
	function dontShow() {
		quitaListeners();
		p1.alpha = p2.alpha = p3.alpha = p4.alpha = p5.alpha = p6.alpha = p7.alpha = p8.alpha = a1.alpha = a2.alpha = a3.alpha = a4.alpha = a5.alpha = a6.alpha = a7.alpha = a8.alpha = 0;
		bordeMc.alpha = 0;
	}
	
	function Show() {
		poneListeners();
		p1.alpha = p2.alpha = p3.alpha = p4.alpha = p5.alpha = p6.alpha = p7.alpha = p8.alpha = a1.alpha = a2.alpha = a3.alpha = a4.alpha = a5.alpha = a6.alpha = a7.alpha = a8.alpha = 1;
		bordeMc.alpha = 1;
		setMascaraForm();
        dragPosicion(null);
	}
	
	function setMascaraForm() {
		p1.x = 0;
		p1.y = -98;
		p2.x = 67;
		p2.y = -23;
		p3.x = 62;
		p3.y = 23;
		p4.x = 47;
		p4.y = 75;
		p5.x = p1.x;
		p5.y = -p1.y+5;
		p6.x = -p4.x;
		p6.y = p4.y;
		p7.x = -p3.x;
		p7.y = p3.y;
		p8.x = -p2.x;
		p8.y = p2.y;
		a1.x = 75;
		a1.y = -95;
		a2.x = 87;
		a2.y = 5;
		a3.x = 61;
		a3.y = 45;
		a4.x = 33;
		a4.y = 98;
		a5.x = -a4.x;
		a5.y = a4.y;
		a6.x = -a3.x;
		a6.y = a3.y;
		a7.x = -a2.x;
		a7.y = a2.y;
		a8.x = -a1.x;
		a8.y = a1.y;
	}
		
	private function dragPunto(event:MouseEvent){ 
		addEventListener(Event.ENTER_FRAME,  dragPosicion);
		event.target.startDrag();
	}
	
	private function dragPosicion(event:Event){
		maskMc.graphics.clear();
		bordeMc.graphics.clear();
		dibujaMask();
		dibujaBorde();
	}
	
	
	private function stopDragPunto(event:MouseEvent){ 
		removeEventListener(Event.ENTER_FRAME,  dragPosicion);
		event.target.stopDrag();
	}
	
	private function dibujaMask(){
	
	maskMc.graphics.lineStyle(1, 0x000000, .5);
	maskMc.graphics.beginFill( 0x009900, .5);
	maskMc.graphics.moveTo(p1.x, p1.y);
	maskMc.graphics.curveTo(a1.x, a1.y, p2.x, p2.y);
	maskMc.graphics.curveTo(a2.x, a2.y, p3.x, p3.y);
	maskMc.graphics.curveTo(a3.x, a3.y, p4.x, p4.y);
	maskMc.graphics.curveTo(a4.x, a4.y, p5.x, p5.y);
	maskMc.graphics.curveTo(a5.x, a5.y, p6.x, p6.y);
	maskMc.graphics.curveTo(a6.x, a6.y, p7.x, p7.y);
	maskMc.graphics.curveTo(a7.x, a7.y, p8.x, p8.y);
	maskMc.graphics.curveTo(a8.x, a8.y, p1.x, p1.y);
	maskMc.graphics.endFill();

	}
	
	private function dibujaBorde(){ 
	
	bordeMc.graphics.lineStyle(3, 0xB7B6B4, 1);
	bordeMc.graphics.moveTo(p1.x, p1.y); 
	bordeMc.graphics.curveTo(a1.x, a1.y, p2.x, p2.y);
	bordeMc.graphics.curveTo(a2.x, a2.y, p3.x, p3.y); 
	bordeMc.graphics.curveTo(a3.x, a3.y, p4.x, p4.y);
	bordeMc.graphics.curveTo(a4.x, a4.y, p5.x, p5.y);
	bordeMc.graphics.curveTo(a5.x, a5.y, p6.x, p6.y);
	bordeMc.graphics.curveTo(a6.x, a6.y, p7.x, p7.y);
	bordeMc.graphics.curveTo(a7.x, a7.y, p8.x, p8.y);
	bordeMc.graphics.curveTo(a8.x, a8.y, p1.x, p1.y);
	bordeMc.graphics.endFill();
	}
		
	}
	
}