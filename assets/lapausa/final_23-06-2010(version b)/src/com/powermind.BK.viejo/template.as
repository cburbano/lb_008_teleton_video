﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class seleccionaImagen extends MovieClip{
		
		function seleccionaImagen():void{
			
		}
		
		function init() {
			visible = true;
			TweenMax.to(this, 1.5, {autoAlpha:1, ease:Expo.easeOut});
		}
		
		function quitaMovie():void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente});
		}
		
		function initSiguiente():void {
			var padre = MovieClip(parent);
			visible = false;
			padre.seleccionaImagen.init();
		}
	}
}