﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	public class elige4Caras extends MovieClip{
		
		var caraUno:Personaje = new Personaje();
		var caraDos:Personaje = new Personaje();
		var caraTres:Personaje = new Personaje();
		var caraCuatro:Personaje = new Personaje();
		var caraCinco:Personaje = new Personaje();
		var prop1:Number;
		var prop2:Number;
		var prop3:Number;
		var prop4:Number;
		
		var height1:Number;
		var height2:Number;
		var height3:Number;
		var height4:Number;
		
		function elige4Caras():void{
			height1 =cara1.inUno.height;
			height2 =cara2.inDos.height;
			height3 =cara3.inTres.height;
			height4 =cara4.inCuatro.height;
			
			prevBtn.visible = false;
			prevBtn.alpha = 0;
			eligeUnoBtn.addEventListener(MouseEvent.CLICK, eligeUno);
			eligeDosBtn.addEventListener(MouseEvent.CLICK, eligeDos);
			eligeTresBtn.addEventListener(MouseEvent.CLICK, eligeTres);
			eligeCuatroBtn.addEventListener(MouseEvent.CLICK, eligeCuatro);
			prevBtn.addEventListener(MouseEvent.CLICK, prevVideo);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
			atrasBtn.addEventListener(MouseEvent.CLICK, atrasFunc);
		}
		
		function revisaCabezas() {
			if(caraUno.creada) {
				prop1 = height1/caraUno.cabeza.height;
				cara1.inUno.visible = false;
				cara1.nombreTxt.text = caraUno.nombre;
				caraUno.cabeza.scaleY = caraUno.cabeza.scaleX = prop1;
				caraUno.boca.scaleY = caraUno.boca.scaleX = prop1;
				caraUno.creada = false;
				caraUno.checka = true;
				if(cara1.numChildren ==3)
					cara1.removeChildAt(2);
				cara1.addChild(caraUno.cabeza);
			}
			if(caraDos.creada) {
				prop2 = height2/caraDos.cabeza.height;
				cara2.inDos.visible = false;
				cara2.nombreTxt.text = caraDos.nombre;
				caraDos.cabeza.scaleY = caraDos.cabeza.scaleX = prop2;
				caraDos.boca.scaleY = caraDos.boca.scaleX = prop2;
				caraDos.creada = false;
				caraDos.checka = true;
				caraCinco.cabeza.scaleY = caraCinco.cabeza.scaleX = prop2;
				caraCinco.boca.scaleY = caraCinco.boca.scaleX = prop2;
				caraCinco.creada = false;
				caraCinco.checka = true;
				if(cara2.numChildren ==3)
					cara2.removeChildAt(2);
				cara2.addChild(caraDos.cabeza);
			}
			if(caraTres.creada) {
				prop3 = height3/caraTres.cabeza.height;
				cara3.inTres.visible = false;
				cara3.nombreTxt.text = caraTres.nombre;
				caraTres.cabeza.scaleY = caraTres.cabeza.scaleX = prop3;
				caraTres.boca.scaleY = caraTres.boca.scaleX = prop3;
				caraTres.creada = false;
				caraTres.checka = true;
				if(cara3.numChildren ==3)
					cara3.removeChildAt(2);
				cara3.addChild(caraTres.cabeza);
			}
			if(caraCuatro.creada) {
				prop4 = height4/caraCuatro.cabeza.height;
				cara4.inCuatro.visible = false;
				if(cara4.numChildren ==3)
					cara4.removeChildAt(2);
				cara4.nombreTxt.text = caraCuatro.nombre;
				caraCuatro.cabeza.scaleY = caraCuatro.cabeza.scaleX = prop4;
				caraCuatro.boca.scaleY = caraCuatro.boca.scaleX = prop4;
				caraCuatro.creada = false;
				caraCuatro.checka = true;
				cara4.addChild(caraCuatro.cabeza);
			}
			if(revisaCaras() == 4) {
				prevBtn.visible = true;
				TweenMax.to(prevBtn, .75, {autoAlpha:1, ease:Expo.easeOut});
		
			}
		}
		
		function revisaCaras():Number{
			var total:Number=0;
			
			if(caraUno.checka)
				total++;
			if(caraDos.checka)
				total++;
			if(caraTres.checka)
				total++;
			if(caraCuatro.checka)
				total++;
			return total;
		}
		
		function prevVideo(e:MouseEvent) {
			quitaMovie(4,5);
		}
		
		function atrasFunc(e:MouseEvent) {
			caraUno = new Personaje();
			caraDos = new Personaje();
			caraTres = new Personaje();
			caraCuatro = new Personaje();
			caraCinco = new Personaje();
			quitaMovie(0, 0);
		}
		
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
		}
		
		function eligeUno(e:MouseEvent):void {
			quitaMovie(1,4);
		}
		
		function eligeDos(e:MouseEvent):void {
			quitaMovie(2,4);
		}
		
		function eligeTres(e:MouseEvent):void {
			quitaMovie(3,4);
		}
		
		function eligeCuatro(e:MouseEvent):void {
			quitaMovie(4,4);
		}
		
		function init() {
			prevBtn.visible = false;
			prevBtn.alpha = 0;
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut});
		}
		
		function quitaMovie(personaje:Number, modo:Number):void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente, onCompleteParams:[personaje, modo]});
		}
		
		function initSiguiente(personaje:Number, modo:Number):void {
			var padre = MovieClip(parent);
			visible = false;
			if(modo == 0)
				padre.eligeVideo.init();
			if(modo == 5)
				padre.prevVideo.init();
			else if((modo >0) && (modo <5))
			{
				padre.seleccionaImagen.init();
				padre.editarCara.pNumero = personaje;
				padre.editarCara.modo = modo;
			}
		} 
	}
}