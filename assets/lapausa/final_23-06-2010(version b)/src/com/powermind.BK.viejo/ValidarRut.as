﻿package com.powermind
{
	public class ValidarRut {
		private var result:Boolean;
		private static  var numero_2:int = 2;
		private static  var numero_7:int = 7;
		
		private var rut:String;
			
		public function ValidarRut() {
			
		}
		
		public function validar(a:String):Boolean {
			rut=a;
			result = false;
			var es_valido:Boolean;
	
			var numero_rut = '';
			var digito_a_verificar = '';
			
			var patron:RegExp = /-/;
			numero_rut = rut.replace(patron, "");
			digito_a_verificar = numero_rut.substr(-1);
			numero_rut = numero_rut.slice(0, -1);
			
			var numero_rut_invertido:Array = numero_rut.split("");
			numero_rut_invertido.reverse();
			var mult_dig_por_serie:Array = new Array();
			var iterador:Number = numero_2;
			
			for (var i:int = 0; i < numero_rut_invertido.length; i++) {
				if (iterador >= numero_7 + 1) {
					iterador = numero_2;
				}
				var resultado:Number = Number(numero_rut_invertido[i] * iterador);
				mult_dig_por_serie.push(resultado);
				iterador++;
			}
			//
			var suma_productos:Number = 0;
			for (var j:int = 0; j < mult_dig_por_serie.length; j++) {
				suma_productos = suma_productos + mult_dig_por_serie[j];
			}
			var modulo11:Number = suma_productos % 11;
			var digito_verificador:Number = 11 - modulo11;
			var digito_verificador_real = '';
			switch (digito_verificador) {
				case 11 :
					digito_verificador_real = '0';
					break;
				case 10 :
					digito_verificador_real = 'k';
					break;
				default :
					digito_verificador_real = String(digito_verificador);
					break;
			}
			if (String(digito_a_verificar) == digito_verificador_real) {
				result = true;
			}
			return result;
		}
	}
}