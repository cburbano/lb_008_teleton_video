﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class eligeVideo extends MovieClip{
		
		function eligeVideo():void{
			eligeClase.addEventListener(MouseEvent.CLICK, clickClase);
			eligeBalance.addEventListener(MouseEvent.CLICK, clickBalance);
			eligePracticante.addEventListener(MouseEvent.CLICK, clickPracticante);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
		}
		
		function clickClase(e:MouseEvent):void {
			MovieClip(parent).prevVideo.videoString = "clase";
			MovieClip(parent).eligeCara.videos = "clase";
			MovieClip(parent).prevVideo.personajes = 2;
			MovieClip(parent).enviarAmigo.video = 1;
			quitaMovie(2);
		}
		
		function clickBalance(e:MouseEvent):void {
			MovieClip(parent).prevVideo.videoString = "balance";
			MovieClip(parent).prevVideo.personajes = 4;
			MovieClip(parent).enviarAmigo.video = 2;
			quitaMovie(4);
		}
		
		function clickPracticante(e:MouseEvent):void {
			MovieClip(parent).prevVideo.videoString = "practicante";
			MovieClip(parent).eligeCara.videos = "practicante";
			MovieClip(parent).prevVideo.personajes = 2;
			MovieClip(parent).enviarAmigo.video = 3;
			quitaMovie(2);
		}
		
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
		}
		
		function init() {
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut});
		}
		
		function quitaMovie(numero:Number):void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente, onCompleteParams:[numero]});
		}
		
		function initSiguiente(numero:Number):void {
		
			var padre = MovieClip(parent);
			visible = false;
			padre.statusMc.posFlecha(3);
			if(numero == 2) {
				padre.eligeCara.init();
			}
			else {
				padre.elige4Caras.init();
			}
		}
		
	}
}