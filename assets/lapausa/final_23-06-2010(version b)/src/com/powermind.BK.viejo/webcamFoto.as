﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	import flash.media.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class webcamFoto extends MovieClip{

		var camara:Camera;
		var video:Video;

		
		function webcamFoto():void{
			capturarBtn.addEventListener(MouseEvent.CLICK, capturaFoto);
			capturarBtn.buttonMode = true;
			capturarBtn.mouseChildren = false;
			siguienteBtn.visible = false;
			siguienteBtn.addEventListener(MouseEvent.CLICK, cargaFinal);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
			atrasBtn.addEventListener(MouseEvent.CLICK, atrasFunc);
		}
		
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
		}
		
		function atrasFunc(e:MouseEvent) {
			nuevaCap(null);
			quitaMovie();
		}
		
		function initCam() {
			camara = Camera.getCamera();
			
			camara.setMode(320,240,10);
			if(camara != null){
				video = new Video(320,240);
				video.attachCamera(camara);
				video.x = -video.width/2;
				video.y = -video.height/2;
				captura.addChild(video);
			}
		}
		
		function compruebaCamara():String {
			if (Camera.getCamera()) 
				{ 
					return "sip";
				}
				else 
				{ 
					return "nop"; 
				}
		}
		
		function capturaFoto(e:MouseEvent) {
			var fotoB:BitmapData = new BitmapData(video.width, video.height);
			
            fotoB.draw(video);
			var foto:Bitmap = new Bitmap(fotoB);
			var fotoFinal:Bitmap = new Bitmap(fotoB);
			foto.smoothing = true;
			foto.x = -foto.width/2;
			foto.y = -foto.height/2;
			fotoFinal.x = foto.x;
			fotoFinal.y = foto.y;
			captura.addChild(foto);
			if(MovieClip(parent).editarCara.mascaraEditar.loaderImagen.loadImg.numChildren == 1)
				MovieClip(parent).editarCara.mascaraEditar.loaderImagen.loadImg.removeChildAt(0);
			MovieClip(parent).editarCara.mascaraEditar.loaderImagen.loadImg.addChild(fotoFinal);
			capturarBtn.inCap.texto.text = capturarBtn.inCap.textop.text = "Nueva Captura";
			capturarBtn.removeEventListener(MouseEvent.CLICK, capturaFoto);
			capturarBtn.addEventListener(MouseEvent.CLICK, nuevaCap);
			siguienteBtn.visible = true;

		}
		
		function nuevaCap(e:MouseEvent) {
			siguienteBtn.visible = false;
			captura.removeChildAt(captura.numChildren-1);
			capturarBtn.inCap.texto.text = capturarBtn.inCap.textop.text = "Capturar";
			capturarBtn.removeEventListener(MouseEvent.CLICK, nuevaCap);
			capturarBtn.addEventListener(MouseEvent.CLICK, capturaFoto);
		}
		
		function cargaFinal(e:MouseEvent) {
			camara.muted;
			quitaMovie();
		}
		
		function init() {
			nuevaCap(null);
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut, onComplete:initCam});
		}
		
		function quitaMovie():void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente});
		}
		
		function initSiguiente():void {
			var padre = MovieClip(parent);
			visible = false;
			padre.editarCara.init();
		}
	}
}