﻿package com.powermind{
	
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class ErrorBox extends MovieClip{
			
		public function ErrorBox(titulo:String, descripcion:String):void{
			var p:Point = new Point(0,0);
			
			this.x = this.localToGlobal(p).x;
			this.y = this.localToGlobal(p).y;
			
			fullBg(3000, 2000);
			cerrar_btn.addEventListener(MouseEvent.CLICK, cierra);
			visible = true;
			cerrar_btn.enabled = true;
			alpha = 0;
			titulo_txt.text = titulo;
			descripcion_txt.text = descripcion;
			
			TweenMax.to(this, 1, {delay:.5, autoAlpha:1, ease:Expo.easeOut});
		}
		
		function fullBg(ancho:Number, alto:Number) {
			bg.width = ancho;
			bg.height = alto;
			bg.x = -bg.width/2;
			bg.y = -bg.height/2;
			}
		
		function cierra(e:MouseEvent):void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:closeThis});
			cerrar_btn.removeEventListener(MouseEvent.CLICK, cierra);
			cerrar_btn.enabled = false;
		}
		
		function closeThis():void {
			visible = false;
			this.parent.removeChild(this);
		}
	}
}