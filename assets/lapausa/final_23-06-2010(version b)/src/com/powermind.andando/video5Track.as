﻿package com.powermind {
	
	import fl.video.*;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.geom.*;
	import flash.text.*;
	import flash.display.Bitmap;
	
	import flash.display.BitmapData;
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	public class video5Track extends MovieClip {
		
		public var player:FLVPlayback = new FLVPlayback();
		
		var c1:MovieClip = new MovieClip();
		var c2:MovieClip = new MovieClip();
		var c3:MovieClip = new MovieClip();
		var c4:MovieClip = new MovieClip();
		var c5:MovieClip = new MovieClip();
		
		var b1:MovieClip = new MovieClip();
		var b2:MovieClip = new MovieClip();
		var b3:MovieClip = new MovieClip();
		var b4:MovieClip = new MovieClip();
		var b5:MovieClip = new MovieClip();
		
		var mascara:MovieClip = new MovieClip();
		var intro:MovieClip = new MovieClip();
		
		var distanciaBase1:Number = 0
		var distanciaBase2:Number = 0
		var distanciaBase3:Number = 0
		var distanciaBase4:Number = 0
		var distanciaBase5:Number = 0
		
		var bit1:Bitmap;
		var bit2:Bitmap;
		var bit3:Bitmap;
		var bit4:Bitmap;
		
		var puntoA1:Point;
		var puntoA2:Point;
		var puntoA3:Point;
		var puntoA4:Point;
		var puntoA5:Point;
		
		var puntoB1:Point;
		var puntoB2:Point;
		var puntoB3:Point;
		var puntoB4:Point;
		var puntoB5:Point;
		
		var PA1:Array;
		var PA2:Array;
		var PA3:Array;
		var PA4:Array;
		var PA5:Array;
		
		var PB1:Array;
		var PB2:Array;
		var PB3:Array;
		var PB4:Array;
		var PB5:Array;
		
		var t1:String;
		var t2:String;
		var t3:String;
		var t4:String;
		
		var suma1:Number;
		var suma2:Number;
		var suma3:Number;
		var suma4:Number;
		
		var ant:Number;
		var abierta1:Number;
		var abierta2:Number;
		var abierta3:Number;
		var abierta4:Number;
		
		var factor:Number = 1.5;
		var ancho:Number = 720;
		var alto:Number = 480;
		var alBoca:Number = 17;
		var xd:Number = 5;
		
		function video5Track(ruta:String, mc1:MovieClip, mc2:MovieClip, mc3:MovieClip, mc4:MovieClip, mc5:MovieClip, b1s:MovieClip, b2s:MovieClip, b3s:MovieClip, b4s:MovieClip, n1:String, n2:String, n3:String, n4:String) {
			
			t1 = n1;
			t2 = n2;
			t3 = n3;
			t4 = n4;
			
			c1 = mc1;
			c2 = mc2;
			c3 = mc3;
			c4 = mc4;
			c5 = mc5;
			
			this.b1 = b1s;
			this.b2 = b2s;
			this.b3 = b3s;
			this.b4 = b4s;
			
			distanciaBase1 = mc1.height;
			distanciaBase2 = mc2.height;
			distanciaBase3 = mc3.height;
			distanciaBase4 = mc4.height;
			distanciaBase5 = mc5.height;
			
			
			mascara.graphics.beginFill(0xFF0000);
			mascara.graphics.moveTo(0,0);
			mascara.graphics.lineTo(ancho,0);
			mascara.graphics.lineTo(ancho,alto);
			mascara.graphics.lineTo(0,alto);
			mascara.graphics.lineTo(0,0);
			mascara.graphics.endFill();
			
			intro.graphics.beginFill(0x000000);
			intro.graphics.moveTo(0,0);
			intro.graphics.lineTo(ancho,0);
			intro.graphics.lineTo(ancho,alto);
			intro.graphics.lineTo(0,alto);
			intro.graphics.lineTo(0,0);
			intro.graphics.endFill();
			
			bit1 = duplicarCara(b1s, c1);
			bit2 = duplicarCara(b2s, c2);
			bit3 = duplicarCara(b3s, c3);
			bit4 = duplicarCara(b4s, c4);
			
			bit1.name = "bit1";
			bit2.name = "bit2";
			bit3.name = "bit3";
			bit4.name = "bit4";
			
			b1.name = "b1";
			b2.name = "b2";
			b3.name = "b3";
			b4.name = "b4";
			
			var dify1:Number = b1.height - bit1.height;
			var dify2:Number = b2.height - bit2.height;
			var dify3:Number = b3.height - bit3.height;
			var dify4:Number = b4.height - bit4.height;
			
			bit1.smoothing = true;
			bit2.smoothing = true;
			bit3.smoothing = true;
			bit4.smoothing = true;
			
			c1.addChild(bit1);
			c2.addChild(bit2);
			c3.addChild(bit3);
			c4.addChild(bit4);
			
			creaIntro(intro);
			
			c1.addChild(b1);
			c2.addChild(b2);
			c3.addChild(b3);
			c4.addChild(b4);
			
			c1.alpha = 0;
			c2.alpha = 0;
			c3.alpha = 0;
			c4.alpha = 0;
			c5.alpha = 0;
			
			c1.getChildByName("bit1").y = c1.getChildByName("bit1").y-dify1/2;
			c2.getChildByName("bit2").y = c2.getChildByName("bit2").y-dify2/2;
			c3.getChildByName("bit3").y = c3.getChildByName("bit3").y-dify3/2;
			c4.getChildByName("bit4").y = c4.getChildByName("bit4").y-dify4/2;
			
			addChild(player);
			
			addChild(c1);
			addChild(c2);
			addChild(c3);
			addChild(c4);
			addChild(c5);
			
			addChild(intro);
			
			addChild(mascara);
			
			this.mask = mascara;
			
			player.width =720;
			player.height = 480;
			player.source = ruta;
			player.align = VideoAlign.TOP_LEFT;
			player.autoPlay = false;
			player.addEventListener(MetadataEvent.CUE_POINT, cuePointListener); 
				
		}
		
		function creaIntro(m:MovieClip) {
			
			var titulo:TextField = new TextField();
			var bajada:TextField = new TextField();
			var perso1:TextField = new TextField();
			var perso2:TextField = new TextField();
			var perso3:TextField = new TextField();
			var perso4:TextField = new TextField();
			
			var ftitulo:TextFormat = new TextFormat("Arial", 30, 0xffffff, true);
			var fperso:TextFormat = new TextFormat("Arial", 20, 0xffffff, true);
			
			titulo.defaultTextFormat = ftitulo;
			bajada.defaultTextFormat = ftitulo;
			perso1.defaultTextFormat = fperso;
			perso2.defaultTextFormat = fperso;
			perso3.defaultTextFormat = fperso;
			perso4.defaultTextFormat = fperso;
			
			titulo.text = '"LOS BALANCES"';
			bajada.text = 'Protagonistas:';
			
			perso1.text = t1;
			perso2.text = t2;
			perso3.text = t3;
			perso4.text = t4;
			
			
			titulo.selectable = false;
			bajada.selectable = false;
			perso1.selectable = false;
			perso2.selectable = false;
			perso3.selectable = false;
			perso4.selectable = false;
			
			titulo.antiAliasType = AntiAliasType.ADVANCED;
			bajada.antiAliasType = AntiAliasType.ADVANCED;
			perso1.antiAliasType = AntiAliasType.ADVANCED;
			perso2.antiAliasType = AntiAliasType.ADVANCED;
			perso3.antiAliasType = AntiAliasType.ADVANCED;
			perso4.antiAliasType = AntiAliasType.ADVANCED;
			
			titulo.autoSize = TextFieldAutoSize.LEFT;
			bajada.autoSize = TextFieldAutoSize.LEFT;
			perso1.autoSize = TextFieldAutoSize.LEFT;
			perso2.autoSize = TextFieldAutoSize.LEFT;
			perso3.autoSize = TextFieldAutoSize.LEFT;
			perso4.autoSize = TextFieldAutoSize.LEFT;
			
			titulo.x = bajada.x = perso1.x = perso2.x = perso3.x = perso4.x = 20;
			
			titulo.y = 280;
			bajada.y = 320;
			
			perso1.y = 360;
			perso2.y = 385;
			perso3.y = 410;
			perso4.y = 435;
			
			m.addChild(titulo);
			m.addChild(bajada);
			m.addChild(perso1);
			m.addChild(perso2);
			m.addChild(perso3);
			m.addChild(perso4);
		}
		
		
		function duplicarCara(m:MovieClip, ref:MovieClip):Bitmap {
			var trans:ColorTransform = new ColorTransform();
			trans.color = 0x000000;
			var bounds:Rectangle = getRealBounds(ref);
			var bmpd:BitmapData = new BitmapData(bounds.width, bounds.height, true, 0xFFFFFF);
			var matrix:Matrix = new Matrix();
			matrix.translate(-bounds.x, -bounds.y);
			bmpd.draw(m, matrix, trans, null, null, true);
			var bmp:Bitmap = new Bitmap(bmpd);
			bmp.smoothing = true;
			bmp.x = -bmp.width/2;
			bmp.y = -bmp.height;
			return bmp;
			
			}
		
		function getRealBounds(displayObject:MovieClip):Rectangle
		{
			var bounds:Rectangle;
			var boundsDispO:Rectangle = displayObject.getBounds(displayObject);
			
			var bitmapData:BitmapData = new BitmapData(int(boundsDispO.width + 0.5), int(boundsDispO.height + 0.5), true, 0);
			
			var matrix:Matrix = new Matrix();
			matrix.translate(-boundsDispO.x, -boundsDispO.y);
			
			bitmapData.draw(displayObject, matrix, new ColorTransform(1, 1, 1, 1, 255, -255, -255, 255 ));
			
			bounds = bitmapData.getColorBoundsRect(0xFF000000, 0xFF000000);
			bounds.x += boundsDispO.x;
			bounds.y += boundsDispO.y;
			
			bitmapData.dispose();
			return bounds;
		}
		
		function cuePointListener(eventObject:MetadataEvent):void { 
			var prop:String; 
			var objeto:Object; 
			
			objeto = eventObject.info.parameters;
			for (prop in objeto)
			{
								
				if(objeto["ver_p1_Transform_Opacity"] == 0){
					c1.alpha = 0;
					}
				
				if(objeto["ver_p2_Transform_Opacity"] == 0){
					c2.alpha = 0;
					}
				
				if(objeto["ver_p3_Transform_Opacity"] == 0){
					c3.alpha = 0;
					}
					
				if(objeto["ver_p4_Transform_Opacity"] == 0){
					c4.alpha = 0;
					}
				
				if(objeto["ver_p5_Transform_Opacity"] == 0){
					c5.alpha = 0;
					}
				
				if (objeto["p1_p1_punto1_Attach Point"]){
					c1.alpha = 1;
					PA1 = objeto["p1_p1_punto1_Attach Point"].split(/,/);
					if(objeto["p1_p1_punto2_Attach Point"]){
					PB1 = objeto["p1_p1_punto2_Attach Point"].split(/,/);
					}
					
					puntoA1= new Point(PA1[0], PA1[1]);
					puntoB1= new Point(PB1[0], PB1[1]);
					var distancia1:Number = Point.distance(puntoA1, puntoB1);
					
					c1.scaleX = (distancia1/distanciaBase1)*factor;
					c1.scaleY = (distancia1/distanciaBase1)*factor;
					
					var xRef1:Number = (PA1[0])-(PB1[0]);
					var yRef1:Number = (PA1[1])-(PB1[1]);
					
					var midX1:Number = Math.abs(PA1[0])+(Math.abs(PB1[0])-Math.abs(PA1[0]))/2
					var midY1:Number = Math.abs(PA1[1])+(Math.abs(PB1[1])-Math.abs(PA1[1]))/2
				
					c1.x = PB1[0];
					c1.y = PB1[1];
					
					if (objeto["boca_p1_Transform_Opacity"]){
						if(objeto["boca_p1_Transform_Opacity"] == 100){
						this.abierta1 = 100;						
						} else 	if(objeto["boca_p1_Transform_Opacity"] == 0){
						this.abierta1 = 0;
						}
					} else {
						
					}
					
						if(this.abierta1 == 100){
						this.abierta1 = 100;
						c1.getChildByName("b1").y = 10*c1.getChildByName("b1").scaleX;
						
						} else 	if(this.abierta1 == 0){
						this.abierta1 = 0;
						c1.getChildByName("b1").y = 0;
						}

					
					var angulo1:Number = -Math.atan2(xRef1, yRef1);
					angulo1 /= Math.PI/180;
					c1.rotation = angulo1-180;
				   
				}
			
				if (objeto["p2_p2_punto1_Attach Point"]){ 
					c2.alpha = 1;
					PA2 = objeto["p2_p2_punto1_Attach Point"].split(/,/);
					if(objeto["p2_p2_punto2_Attach Point"]){
					PB2 = objeto["p2_p2_punto2_Attach Point"].split(/,/);
					}
					if(objeto["ver_p2_Transform_Opacity"] == 0){
					c2.alpha = 0;
					}
					
					puntoA2 = new Point(PA2[0], PA2[1]);
					puntoB2 = new Point(PB2[0], PB2[1]);
					var distancia2:Number = Point.distance(puntoA2, puntoB2);
					
					c2.scaleX = (distancia2/distanciaBase2)*factor;
					c2.scaleY = (distancia2/distanciaBase2)*factor;
					
					var midX2:Number = Math.abs(PA2[0])+(Math.abs(PB2[0])-Math.abs(PA2[0]))/2
					var midY2:Number = Math.abs(PA2[1])+(Math.abs(PB2[1])-Math.abs(PA2[1]))/2
				
					c2.x = PB2[0];
					c2.y = PB2[1];
					
					if (objeto["boca_p2_Transform_Opacity"]){
						if(objeto["boca_p2_Transform_Opacity"] == 100){
						this.abierta2 = 100;						
						} else 	if(objeto["boca_p2_Transform_Opacity"] == 0){
						this.abierta2 = 0;
						}
					}			
					
					if(this.abierta2 == 100){
					this.abierta2 = 100;
					c2.getChildByName("b2").y = 10*c2.getChildByName("b2").scaleX;
					
					} else 	if(this.abierta2 == 0){
					this.abierta2 = 0;
					c2.getChildByName("b2").y = 0;
					}
					
					var xRef2:Number = (PA2[0])-(PB2[0]);
					var yRef2:Number = (PA2[1])-(PB2[1]);
					
					var angulo2:Number = -Math.atan2(xRef2, yRef2);
					angulo2 /= Math.PI/180;
					c2.rotation = angulo2-180;
				}
				
				if (objeto["p3_p3_punto1_Attach Point"]){ 
					c3.alpha = 1;
					PA3 = objeto["p3_p3_punto1_Attach Point"].split(/,/);
					if(objeto["p3_p3_punto2_Attach Point"]){
					PB3 = objeto["p3_p3_punto2_Attach Point"].split(/,/);
					}
					if(objeto["ver_p3_Transform_Opacity"] == 0){
					c3.alpha = 0;
					}
					
					puntoA3 = new Point(PA3[0], PA3[1]);
					puntoB3 = new Point(PB3[0], PB3[1]);
					var distancia3:Number = Point.distance(puntoA3, puntoB3);
					
					c3.scaleX = (distancia3/distanciaBase3)*factor;
					c3.scaleY = (distancia3/distanciaBase3)*factor;
					
					
					var midX3:Number = Math.abs(PA3[0])+(Math.abs(PB3[0])-Math.abs(PA3[0]))/2
					var midY3:Number = Math.abs(PA3[1])+(Math.abs(PB3[1])-Math.abs(PA3[1]))/2
				
					c3.x = PB3[0];
					c3.y = PB3[1];
					
					if (objeto["boca_p3_Transform_Opacity"]){
						if(objeto["boca_p3_Transform_Opacity"] == 100){
						this.abierta3 = 100;						
						} else 	if(objeto["boca_p3_Transform_Opacity"] == 0){
						this.abierta3 = 0;
						}
					}			
					
					if(this.abierta3 == 100){
					this.abierta3 = 100;
					c3.getChildByName("b3").y = 10*c3.getChildByName("b3").scaleX;
					
					} else 	if(this.abierta3 == 0){
					this.abierta3 = 0;
					c3.getChildByName("b3").y = 0;
					}
					
					var xRef3:Number = (PA3[0])-(PB3[0]);
					var yRef3:Number = (PA3[1])-(PB3[1]);
					
					var angulo3:Number = -Math.atan2(xRef3, yRef3);
					angulo3 /= Math.PI/180;
					c3.rotation = angulo3-180;
				   
				}
				
				if (objeto["p4_p4_punto1_Attach Point"]){
					c4.alpha = 1;
					PA4 = objeto["p4_p4_punto1_Attach Point"].split(/,/);
					if(objeto["p4_p4_punto2_Attach Point"]){
					PB4 = objeto["p4_p4_punto2_Attach Point"].split(/,/);
					}
					if(objeto["ver_p4_Transform_Opacity"] == 0){
					c4.alpha = 0;
					}
					
					puntoA4 = new Point(PA4[0], PA4[1]);
					puntoB4 = new Point(PB4[0], PB4[1]);
					var distancia4:Number = Point.distance(puntoA4, puntoB4);
					
					c4.scaleX = (distancia4/distanciaBase4)*factor;
					c4.scaleY = (distancia4/distanciaBase4)*factor;
					
					var midX4:Number = Math.abs(PA4[0])+(Math.abs(PB4[0])-Math.abs(PA4[0]))/2
					var midY4:Number = Math.abs(PA4[1])+(Math.abs(PB4[1])-Math.abs(PA4[1]))/2
				
					c4.x = PB4[0];
					c4.y = PB4[1];
					
					if (objeto["boca_p4_Transform_Opacity"]){
						if(objeto["boca_p4_Transform_Opacity"] == 100){
						this.abierta4 = 100;						
						} else 	if(objeto["boca_p4_Transform_Opacity"] == 0){
						this.abierta4 = 0;
						}
					}			
					
					if(this.abierta4 == 100){
					this.abierta4 = 100;
					c4.getChildByName("b4").y = 10*c4.getChildByName("b4").scaleX;
					
					} else 	if(this.abierta4 == 0){
					this.abierta4 = 0;
					c4.getChildByName("b4").y = 0;
					}
					
					var xRef4:Number = (PA4[0])-(PB4[0]);
					var yRef4:Number = (PA4[1])-(PB4[1]);
					
					var angulo4:Number = -Math.atan2(xRef4, yRef4);
					angulo4 /= Math.PI/180;
					c4.rotation = angulo4-180;
				   
				}
				
				if (objeto["p5_p5_punto1_Attach Point"]){ 
				
					c5.alpha = 1;
					PA5 = objeto["p5_p5_punto1_Attach Point"].split(/,/);
					if(objeto["p5_p5_punto2_Attach Point"]){
					PB5 = objeto["p5_p5_punto2_Attach Point"].split(/,/);
					}
					if(objeto["ver_p5_Transform_Opacity"] == 0){
					c5.alpha = 0;
					}
					
					puntoA5 = new Point(PA5[0], PA5[1]);
					puntoB5 = new Point(PB5[0], PB5[1]);
					var distancia5:Number = Point.distance(puntoA5, puntoB5);
					
					c5.scaleX = (distancia5/distanciaBase5)*1.4;
					c5.scaleY = (distancia5/distanciaBase5)*1.4;
					
					var midX5:Number = Math.abs(PA5[0])+(Math.abs(PB5[0])-Math.abs(PA5[0]))/2
					var midY5:Number = Math.abs(PA5[1])+(Math.abs(PB5[1])-Math.abs(PA5[1]))/2
				
					c5.x = PB5[0];
					c5.y = PB5[1];
					
					var xRef5:Number = (PA5[0])-(PB5[0]);
					var yRef5:Number = (PA5[1])-(PB5[1]);
					
					var angulo5:Number = -Math.atan2(xRef5, yRef5);
					angulo5 /= Math.PI/180;
					c5.rotation = angulo5-180;
				   
				}
				
			}
		}
		
		
		public function playVideo():void {
			player.seek(0);
			player.stop();
			this.intro.alpha = 1;
			TweenMax.to(this.intro, 2, {delay:4, alpha:0, ease:Back.easeOut, onComplete:iniciaVideo});
		}
		
		function iniciaVideo() {
			player.play();
		}
		
		public function stopVideo():void {
			player.stop();
		}
		

	}
	
}