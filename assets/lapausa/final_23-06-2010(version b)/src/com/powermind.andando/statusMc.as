﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class statusMc extends MovieClip{
		
		function statusMc():void{
		
		}
		
		public function posFlecha(pos:Number) {
			bgStatus.visible = false;
			
			switch(pos) {
				case 1:
				TweenMax.to(flechaStatus, 1, {x:-58, y:-194, ease:Expo.easeOut});
				selectOff(eligeVideo);
				selectOff(subeFoto);
				selectOff(enviarAmigo);
				bgStatus.visible = true;
				break;
				
				case 2:
				TweenMax.to(flechaStatus, 1, {x:-40, y:-130, ease:Expo.easeOut});
				selectOn(eligeVideo);
				selectOff(subeFoto);
				selectOff(enviarAmigo);
				break;
				
				case 3:
				TweenMax.to(flechaStatus, 1, {x:-40, y:-32, ease:Expo.easeOut});
				selectOn(subeFoto);
				selectOff(eligeVideo);
				selectOff(enviarAmigo);
				break;
				
				case 4:
				TweenMax.to(flechaStatus, 1, {x:-40, y:104, ease:Expo.easeOut});
				selectOn(enviarAmigo);
				selectOff(subeFoto);
				selectOff(eligeVideo);
				break;
			}
		}
		
		function selectOn(m:MovieClip):void {
			TweenMax.to(m, .5, {glowFilter:{color:0xFFE500, alpha:1, blurX:20, blurY:20}});
		}
		
		function selectOff(m:MovieClip):void {
			TweenMax.to(m, .5, {glowFilter:{color:0xFFE500, alpha:0, blurX:0, blurY:0}});
		}
	}
}