﻿package com.powermind {
	
	import fl.video.*;
	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.net.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	public class ReproductorFinal extends MovieClip {
		
		var video:video5Track;
		var b1:boca = new boca();
		var b2:boca = new boca();
		var b3:boca = new boca();
		var b4:boca = new boca();
		
		var c1:cara = new cara();
		var c2:cara = new cara();
		var c3:cara = new cara();
		var c4:cara = new cara();
		var c5:cara = new cara();
		
		public function ReproductorFinal() {
			cargaVideo();
		}
		
		
		
		function cargaVideo() {
			video = new video5Track("flv/balance.flv", c1, c2, c3, c4, c5, b1, b2, b3, b4, "Aldo", "Carlos", "Aldo", "Carlos");
			addChild(video);
			video.x = this.stage.stageWidth/2 - video.width/2;
			video.y = this.stage.stageHeight/2 - video.height/2-10;
			video.playVideo();
		}

	}
}