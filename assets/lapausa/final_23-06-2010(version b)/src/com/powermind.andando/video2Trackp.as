﻿package com.powermind {
	
	import fl.video.*;
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.geom.*;
	import flash.text.*;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	public class video2Trackp extends MovieClip {
		
		public var player:FLVPlayback = new FLVPlayback();
		
		var c1:MovieClip = new MovieClip();
		var c2:MovieClip = new MovieClip();
		
		var b1:MovieClip = new MovieClip();
		var b2:MovieClip = new MovieClip();
		
		var rosa:Rosa = new Rosa();
		var monitor1:m1 = new m1();
		var monitor2:m2 = new m2();
		var monitor3:m3 = new m3();
		
		var mascara:MovieClip = new MovieClip();
		var intro:MovieClip = new MovieClip();
		
		var distanciaBase1:Number = 0
		var distanciaBase2:Number = 0
		var distanciaBase3:Number = 0
		
		var bit1:Bitmap;
		var bit2:Bitmap;
		
		var puntoA1:Point;
		var puntoA2:Point;
		var puntoA3:Point;
		
		var puntoB1:Point;
		var puntoB2:Point;
		var puntoB3:Point;
		
		var PA1:Array;
		var PA2:Array;
		var PA3:Array;
		
		var PB1:Array;
		var PB2:Array;
		var PB3:Array;
		
		var t1:String;
		var t2:String;
		
		var suma1:Number;
		var suma2:Number;
		var suma3:Number;
		
		var ant:Number;
		var abierta1:Number;
		var abierta2:Number;
		
		var factor:Number = 1.5;
		var ancho:Number = 720;
		var alto:Number = 480;
		var alBoca:Number = 17;
		var xd:Number = 7;
		
		function video2Trackp(ruta:String, mc1:MovieClip, mc2:MovieClip, b1s:MovieClip, b2s:MovieClip, n1:String, n2:String) {
			
			t1 = n1;
			t2 = n2;
			
			c1 = mc1;
			c2 = mc2;
			
			this.b1 = b1s;
			this.b2 = b2s;
			
			distanciaBase1 = mc1.height;
			distanciaBase2 = mc2.height;
			
			mascara.graphics.beginFill(0xFF0000);
			mascara.graphics.moveTo(0,0);
			mascara.graphics.lineTo(ancho,0);
			mascara.graphics.lineTo(ancho,alto);
			mascara.graphics.lineTo(0,alto);
			mascara.graphics.lineTo(0,0);
			mascara.graphics.endFill();
			
			intro.graphics.beginFill(0x000000);
			intro.graphics.moveTo(0,0);
			intro.graphics.lineTo(ancho,0);
			intro.graphics.lineTo(ancho,alto);
			intro.graphics.lineTo(0,alto);
			intro.graphics.lineTo(0,0);
			intro.graphics.endFill();
			
			bit1 = duplicarCara(b1s, c1);
			bit2 = duplicarCara(b2s, c2);
			
			bit1.name = "bit1";
			bit2.name = "bit2";
			
			b1.name = "b1";
			b2.name = "b2";
			
			var dify1:Number = b1.height - bit1.height;
			var dify2:Number = b2.height - bit2.height;
			
			bit1.smoothing = true;
			bit2.smoothing = true;
			
			c1.addChild(bit1);
			c2.addChild(bit2);
			
			creaIntro(intro);
			
			c1.addChild(b1);
			c2.addChild(b2);
			
			c1.alpha = 0;
			c2.alpha = 0;
			
			rosa.alpha = 0;
			monitor1.alpha = 0;
			monitor1.x = -(monitor1.width*.1)/2;
			monitor1.y = -(monitor1.height*.1)+4;
			monitor1.scaleX = monitor1.scaleY = 1.1;
			monitor2.alpha = 0;
			monitor3.alpha = 0;
			monitor3.scaleX = monitor3.scaleY = 1.1;
			
			c1.getChildByName("bit1").y = c1.getChildByName("bit1").y-dify1/2;
			c2.getChildByName("bit2").y = c2.getChildByName("bit2").y-dify2/2;
			
			addChild(player);
			
			addChild(c1);
			addChild(c2);
			addChild(rosa);
			addChild(monitor1);
			addChild(monitor2);
			addChild(monitor3);
			
			addChild(intro);
			
			addChild(mascara);
			
			this.mask = mascara;
			
			player.width =720;
			player.height = 480;
			player.source = ruta;
			player.align = VideoAlign.TOP_LEFT;
			player.autoPlay = false;
			player.addEventListener(MetadataEvent.CUE_POINT, cuePointListener); 
				
		}
		
		function creaIntro(m:MovieClip) {
			
			var titulo:TextField = new TextField();
			var bajada:TextField = new TextField();
			var perso1:TextField = new TextField();
			var perso2:TextField = new TextField();
			
			var ftitulo:TextFormat = new TextFormat("Arial", 30, 0xffffff, true);
			var fperso:TextFormat = new TextFormat("Arial", 20, 0xffffff, true);
			
			titulo.defaultTextFormat = ftitulo;
			bajada.defaultTextFormat = ftitulo;
			perso1.defaultTextFormat = fperso;
			perso2.defaultTextFormat = fperso;
			
			titulo.text = '"LA PRACTICANTE"';
			bajada.text = 'Protagonistas:';
			
			perso1.text = t1;
			perso2.text = t2;
			
			
			titulo.selectable = false;
			bajada.selectable = false;
			perso1.selectable = false;
			perso2.selectable = false;
			
			titulo.antiAliasType = AntiAliasType.ADVANCED;
			bajada.antiAliasType = AntiAliasType.ADVANCED;
			perso1.antiAliasType = AntiAliasType.ADVANCED;
			perso2.antiAliasType = AntiAliasType.ADVANCED;

			titulo.autoSize = TextFieldAutoSize.LEFT;
			bajada.autoSize = TextFieldAutoSize.LEFT;
			perso1.autoSize = TextFieldAutoSize.LEFT;
			perso2.autoSize = TextFieldAutoSize.LEFT;
			
			titulo.x = bajada.x = perso1.x = perso2.x = 20;
			
			titulo.y = 280;
			bajada.y = 320;
			
			perso1.y = 360;
			perso2.y = 385;
						
			m.addChild(titulo);
			m.addChild(bajada);
			m.addChild(perso1);
			m.addChild(perso2);
		}
		
		function duplicarCara(m:MovieClip, ref:MovieClip):Bitmap {
			var trans:ColorTransform = new ColorTransform();
			trans.color = 0x000000;
			var bounds:Rectangle = getRealBounds(ref);
			var bmpd:BitmapData = new BitmapData(bounds.width, bounds.height, true, 0xFFFFFF);
			var matrix:Matrix = new Matrix();
			matrix.translate(-bounds.x, -bounds.y);
			bmpd.draw(m, matrix, trans, null, null, true);
			var bmp:Bitmap = new Bitmap(bmpd);
			bmp.smoothing = true;
			bmp.x = -bmp.width/2;
			bmp.y = -bmp.height;
			return bmp;
			
			}
		
		function getRealBounds(displayObject:MovieClip):Rectangle
		{
			var bounds:Rectangle;
			var boundsDispO:Rectangle = displayObject.getBounds(displayObject);
			
			var bitmapData:BitmapData = new BitmapData(int(boundsDispO.width + 0.5), int(boundsDispO.height + 0.5), true, 0);
			
			var matrix:Matrix = new Matrix();
			matrix.translate(-boundsDispO.x, -boundsDispO.y);
			
			bitmapData.draw(displayObject, matrix, new ColorTransform(1, 1, 1, 1, 255, -255, -255, 255 ));
			
			bounds = bitmapData.getColorBoundsRect(0xFF000000, 0xFF000000);
			bounds.x += boundsDispO.x;
			bounds.y += boundsDispO.y;
			
			bitmapData.dispose();
			return bounds;
		}
		
		function cuePointListener(eventObject:MetadataEvent):void { 
			var prop:String; 
			var objeto:Object; 
			
			objeto = eventObject.info.parameters;
			for (prop in objeto)
			{
								
				if(objeto["ver_p1_Transform_Opacity"] == 0){
					c1.alpha = 0;
					}
				
				if(objeto["ver_p2_Transform_Opacity"] == 0){
					c2.alpha = 0;
					}
				
				if(objeto["ver_rosa_Transform_Opacity"] == 0){
					rosa.alpha = 0;
					}
				if(objeto["ver_rosa_Transform_Opacity"] == 1){
					rosa.alpha = 1;
					}
				if(objeto["ver_m1_Transform_Opacity"] == 0){
					monitor1.alpha = 0;
					}
				if(objeto["ver_m2_Transform_Opacity"] == 0){
					monitor2.alpha = 0;
					}
				if(objeto["ver_m3_Transform_Opacity"] == 0){
					monitor3.alpha = 0;
					}
				if(objeto["ver_m1_Transform_Opacity"] == 1){
					monitor1.alpha = 1;
					}
				if(objeto["ver_m2_Transform_Opacity"] == 1){
					monitor2.alpha = 1;
					}
				if(objeto["ver_m3_Transform_Opacity"] == 1){
					monitor3.alpha = 1;
					}
				
				if (objeto["p1_p1_punto1_Attach Point"]){
					c1.alpha = 1;
					PA1 = objeto["p1_p1_punto1_Attach Point"].split(/,/);
					if(objeto["p1_p1_punto2_Attach Point"]){
					PB1 = objeto["p1_p1_punto2_Attach Point"].split(/,/);
					}
					if(objeto["ver_p1_Transform_Opacity"] == 0){
					c1.alpha = 0;
					}
					
					puntoA1= new Point(PA1[0], PA1[1]);
					puntoB1= new Point(PB1[0], PB1[1]);
					var distancia1:Number = Point.distance(puntoA1, puntoB1);
					
					c1.scaleX = (distancia1/distanciaBase1)*factor;
					c1.scaleY = (distancia1/distanciaBase1)*factor;
					
					var xRef1:Number = (PA1[0])-(PB1[0]);
					var yRef1:Number = (PA1[1])-(PB1[1]);
					
					var midX1:Number = Math.abs(PA1[0])+(Math.abs(PB1[0])-Math.abs(PA1[0]))/2
					var midY1:Number = Math.abs(PA1[1])+(Math.abs(PB1[1])-Math.abs(PA1[1]))/2
				
					c1.x = PB1[0];
					c1.y = PB1[1];
					rosa.scaleX = rosa.scaleY = c1.scaleX*.4;
					rosa.x = c1.x+rosa.width/5.5;
					rosa.y = c1.y-rosa.height/2;
					
					if (objeto["boca_p1_Transform_Opacity"]){
						if(objeto["boca_p1_Transform_Opacity"] == 100){
						this.abierta1 = 100;						
						} else 	if(objeto["boca_p1_Transform_Opacity"] == 0){
						this.abierta1 = 0;
						}
					} else {
						
					}
					
						if(this.abierta1 == 100){
						this.abierta1 = 100;
						c1.getChildByName("b1").y = 10*c1.getChildByName("b1").scaleX;
						
						} else 	if(this.abierta1 == 0){
						this.abierta1 = 0;
						c1.getChildByName("b1").y = 0;
						}

					
					var angulo1:Number = -Math.atan2(xRef1, yRef1);
					angulo1 /= Math.PI/180;
					c1.rotation = angulo1-180;
				   
				}
			
				if (objeto["p2_p2_punto1_Attach Point"]){ 
					c2.alpha = 1;
					PA2 = objeto["p2_p2_punto1_Attach Point"].split(/,/);
					if(objeto["p2_p2_punto2_Attach Point"]){
					PB2 = objeto["p2_p2_punto2_Attach Point"].split(/,/);
					}
					if(objeto["ver_p2_Transform_Opacity"] == 0){
					c2.alpha = 0;
					}
					
					puntoA2 = new Point(PA2[0], PA2[1]);
					puntoB2 = new Point(PB2[0], PB2[1]);
					var distancia2:Number = Point.distance(puntoA2, puntoB2);
					
					c2.scaleX = (distancia2/distanciaBase2)*factor;
					c2.scaleY = (distancia2/distanciaBase2)*factor;
					
					var midX2:Number = Math.abs(PA2[0])+(Math.abs(PB2[0])-Math.abs(PA2[0]))/2
					var midY2:Number = Math.abs(PA2[1])+(Math.abs(PB2[1])-Math.abs(PA2[1]))/2
				
					c2.x = PB2[0];
					c2.y = Number(PB2[1])+15;
					
					if (objeto["boca_p2_Transform_Opacity"]){
						if(objeto["boca_p2_Transform_Opacity"] == 100){
						this.abierta2 = 100;						
						} else 	if(objeto["boca_p2_Transform_Opacity"] == 0){
						this.abierta2 = 0;
						}
					}			
					
					if(this.abierta2 == 100){
					this.abierta2 = 100;
					c2.getChildByName("b2").y = 10*c2.getChildByName("b2").scaleX;
					
					} else 	if(this.abierta2 == 0){
					this.abierta2 = 0;
					c2.getChildByName("b2").y = 0;
					}
					
					var xRef2:Number = (PA2[0])-(PB2[0]);
					var yRef2:Number = (PA2[1])-(PB2[1]);
					
					var angulo2:Number = -Math.atan2(xRef2, yRef2);
					angulo2 /= Math.PI/180;
					c2.rotation = angulo2-180;
				}
				
			}
		}
		
		
		public function playVideo():void {
			player.seek(0);
			player.stop();
			this.intro.alpha = 1;
			TweenMax.to(this.intro, 2, {delay:4, alpha:0, ease:Back.easeOut, onComplete:iniciaVideo});
		}
		
		function iniciaVideo() {
			player.play();
		}
		
		public function stopVideo():void {
			player.stop();
		}
		

	}
	
}