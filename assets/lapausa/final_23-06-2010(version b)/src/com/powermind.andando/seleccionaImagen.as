﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	import flash.media.Camera;
	import flash.utils.*;
	
	import flash.system.JPEGLoaderContext;
	import flash.system.Security;
	
	import flash.external.ExternalInterface;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class seleccionaImagen extends MovieClip{
		var archivo:FileReference = new FileReference();
		var loader:Loader;
		var bitmap:Bitmap;
		var direccion:String;
		
		function seleccionaImagen():void{
			pcBtn.addEventListener(MouseEvent.CLICK, cargaImagen);
			pcBtn.addEventListener(MouseEvent.ROLL_OVER, inGlow);
			pcBtn.addEventListener(MouseEvent.ROLL_OUT, outGlow);
			pcBtn.buttonMode = true;
			
			facebookBtn.addEventListener(MouseEvent.CLICK, cargaFacebook);
			facebookBtn.addEventListener(MouseEvent.ROLL_OVER, inGlow);
			facebookBtn.addEventListener(MouseEvent.ROLL_OUT, outGlow);
			facebookBtn.buttonMode = true;
			testCam();
			
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
			archivo.addEventListener(Event.SELECT, selectImagen);
			atrasBtn.addEventListener(MouseEvent.CLICK, atrasFunc);
			
			if (ExternalInterface.available){
				ExternalInterface.addCallback("sendToActionScript", receivedFromJavaScript);
			}
			
		}
		
		function testCam() {
			var camarita:Camera;
			camarita = Camera.getCamera();
			if(camarita != null) {
				camaraBtn.addEventListener(MouseEvent.CLICK, cargaCamara);
				camaraBtn.addEventListener(MouseEvent.ROLL_OVER, inGlow);
				camaraBtn.addEventListener(MouseEvent.ROLL_OUT, outGlow);
				camaraBtn.buttonMode = true;
			}
			else {
				camaraBtn.alpha = .5;
			}
			
		}
		function inGlow(e:MouseEvent):void {
			selectOn(MovieClip(e.target));
		}
		
		function outGlow(e:MouseEvent):void {
			selectOff(MovieClip(e.target));
		}
		
		function selectOn(m:MovieClip):void {
			TweenMax.to(m, 1, {glowFilter:{color:0x99CC00, alpha:1, blurX:50, blurY:50}});
		}
		
		function selectOff(m:MovieClip):void {
			TweenMax.to(m, 1, {glowFilter:{color:0x99CC00, alpha:0, blurX:0, blurY:0}});
		}
		
		function cargaImagen(e:MouseEvent) {
			var tipoImagen:FileFilter = new FileFilter("Imágenes","*.jpg; *.jpeg; *.png; *.gif");
			var tipos:Array = new Array();
			tipos.push(tipoImagen);
			archivo.browse(tipos);
		}
		
		function atrasFunc(e:MouseEvent) {
			if(MovieClip(parent).prevVideo.personajes == 2)
				quitaMovie(3);
			else {
				quitaMovie(4);
			}
		}
		
		function cargaCamara(e:MouseEvent) {
			quitaMovie(2);
		}
		
		private function cargaFacebook(e:MouseEvent):void {
			if (ExternalInterface.available) ExternalInterface.call("mostrarDivFacebook");
			//Funcion que llama a la carga de imagenes de Facebook
		}
		private function receivedFromJavaScript(value:String):void {
			direccion = value;
			trace('version 5');
			trace(direccion);
			//var tiempo:Timer = new Timer(100);
			//tiempo.addEventListener(TimerEvent.TIMER_COMPLETE, cargaImagenT);
			
			//copia de la funcion cargaImagenT(...)
			loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, cargaFinal);
			var peticion:URLRequest = new URLRequest(direccion);
			
			//generar un contexto de seguridad
			Security.loadPolicyFile('http://api.facebook.com/crossdomain.xml');
			var contexto:JPEGLoaderContext = new JPEGLoaderContext();
			contexto.checkPolicyFile = true;
			
			loader.load(peticion,contexto);
        }
		
		/*function cargaImagenT(e:TimerEvent) {
			loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, cargaFinal);
			var peticion:URLRequest = new URLRequest(direccion);
			loader.load(peticion);
		}*/
		
		function selectImagen(e:Event):void {
			
			archivo.addEventListener(Event.COMPLETE, cargaCompleta);
			archivo.load();
			
		}
		
		function cargaCompleta(e:Event):void {
	
			var tempFileRef : FileReference = FileReference (e.target) ;
			loader = new Loader () ;
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, cargaFinal) ;
			loader.loadBytes (archivo.data) ;
			
		}
		
		function cargaFinal(e:Event) {
			bitmap = Bitmap (e.target.content) ;
			bitmap.smoothing = true;
			bitmap.x = -bitmap.width/2;
			bitmap.y = -bitmap.height/2;
			if(MovieClip(parent).editarCara.mascaraEditar.loaderImagen.loadImg.numChildren == 1)
				MovieClip(parent).editarCara.mascaraEditar.loaderImagen.loadImg.removeChildAt(0);
			MovieClip(parent).editarCara.mascaraEditar.loaderImagen.loadImg.addChild(bitmap);
			quitaMovie(1);
		}
		
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
		}
		
		function init() {
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut});
		}
		
		function quitaMovie(i:Number):void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente, onCompleteParams:[i]});
		}
		
		function initSiguiente(i:Number):void {
			var padre = MovieClip(parent);
			visible = false;
			
			switch (i) {
				
				case 1:
				padre.editarCara.init();
				break;
				
				case 2:
				padre.webcamFoto.init();
				break;
				
				case 3:
				padre.eligeCara.init();
				break;
				
				case 4:
				padre.elige4Caras.init();
				break;
				
			}
		}
	}
}