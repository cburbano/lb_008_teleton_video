﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	import flash.utils.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	import joe.*;
	
	import com.adobe.images.*;

	public class prevVideo extends MovieClip{
		
		public var videoString:String;
		public var personajes:Number;
				
		var personas:Array = new Array();
		var bocas:Array = new Array();
		var rutaBase:String = "http://localhost/lapausa/";
		var videos:video5Track;
		var videop:video2Trackp;
		var videoc:video2Trackc;
		
		public var yaseHizo:Boolean = false;
		
		var n1:String;
		var n2:String;
		var n3:String;
		var n4:String;
		
		var b1:MovieClip = new MovieClip();
		var b2:MovieClip = new MovieClip();
		var b3:MovieClip = new MovieClip();
		var b4:MovieClip = new MovieClip();
		var b5:MovieClip = new MovieClip();
		
		var c1:MovieClip = new MovieClip();
		var c2:MovieClip = new MovieClip();
		var c3:MovieClip = new MovieClip();
		var c4:MovieClip = new MovieClip();
		var c5:MovieClip = new MovieClip();
		
		var tiempo:Timer = new Timer(5000, 1);
		
		function prevVideo():void{
			botonAtras.addEventListener(MouseEvent.CLICK, atrasFuncion);
			listoBtn.visible = false;
			listoBtn.addEventListener(MouseEvent.CLICK, enviaMovie);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
			tiempo.addEventListener(TimerEvent.TIMER_COMPLETE, tiempoCompletado);
		}
		
		function atrasFuncion(e:MouseEvent) {
			quitaMovie(2);
		}
		
		function tiempoCompletado(e:TimerEvent) {
			listoBtn.visible = true;
		}
		function enviaMovie(e:MouseEvent)
		{
			if(videoString == "balance")
			{
			videos.stopVideo();
			}
			else if(videoString == "clase")
			{
			videoc.stopVideo();
			}
			else if(videoString == "practicante")
			{
			videop.stopVideo();
			}
			quitaMovie(1);
			listoBtn.removeEventListener(MouseEvent.CLICK, enviaMovie);
		}
		
		function repiteMovie(e:MouseEvent)
		{
			if(videoString == "balance")
			{
			videos.stopVideo();
			}
			else if(videoString == "clase")
			{
			videoc.stopVideo();
			}
			else if(videoString == "practicante")
			{
			videop.stopVideo();
			}
			quitaMovie(2);
			listoBtn.removeEventListener(MouseEvent.CLICK, repiteMovie);
		}
		
		function totalCaras()
		{	
		yaseHizo = true;
			if(personajes == 4)
			{
				c1.addChild(MovieClip(parent).elige4Caras.caraDos.cabeza);
				c2.addChild(MovieClip(parent).elige4Caras.caraUno.cabeza);
				c3.addChild(MovieClip(parent).elige4Caras.caraTres.cabeza);
				c4.addChild(MovieClip(parent).elige4Caras.caraCuatro.cabeza);
				c5.addChild(MovieClip(parent).elige4Caras.caraCinco.cabeza);
				
				b1.addChild(MovieClip(parent).elige4Caras.caraDos.boca);
				b2.addChild(MovieClip(parent).elige4Caras.caraUno.boca);
				b3.addChild(MovieClip(parent).elige4Caras.caraTres.boca);
				b4.addChild(MovieClip(parent).elige4Caras.caraCuatro.boca);
				
				n1 = MovieClip(parent).elige4Caras.caraUno.nombre;
				n2 = MovieClip(parent).elige4Caras.caraDos.nombre;
				n3 = MovieClip(parent).elige4Caras.caraTres.nombre;
				n4 = MovieClip(parent).elige4Caras.caraCuatro.nombre;
				
				personas[0] = c1;
				personas[1] = c2;
				personas[2] = c3;
				personas[3] = c4;
				personas[4] = c5;
				
				bocas[0] = b1;
				bocas[1] = b2;
				bocas[2] = b3;
				bocas[3] = b4;
				
				for(var i:int=0; i<5; i++){
					personas[i].getChildAt(0).y = -personas[i].getChildAt(0).height/2; 
				}
			
			for(var j:int=0; j<4; j++){
					bocas[j].getChildAt(0).y = -bocas[j].getChildAt(0).height/2;
				}
				
			}
				else if(personajes == 2)
			{
				c1.addChild(MovieClip(parent).eligeCara.caraUno.cabeza);
				c2.addChild(MovieClip(parent).eligeCara.caraDos.cabeza);
				
				b1.addChild(MovieClip(parent).eligeCara.caraUno.boca);
				b2.addChild(MovieClip(parent).eligeCara.caraDos.boca);
				
				n1 = MovieClip(parent).eligeCara.caraUno.nombre;
				n2 = MovieClip(parent).eligeCara.caraDos.nombre;
				
				personas[0] = c1;
				personas[1] = c2;

				bocas[0] = b1;
				bocas[1] = b2;
				
				
				personas[0].getChildAt(0).y = -personas[i].getChildAt(0).height/2;
				personas[1].getChildAt(0).y = -personas[i].getChildAt(0).height/2; 
				
				bocas[0].getChildAt(0).y = -bocas[j].getChildAt(0).height/2;
				bocas[1].getChildAt(0).y = -bocas[j].getChildAt(0).height/2;
			}
		}
		
		function cargaVideo() {
			tiempo.start();
			if(videoString == "balance")
			{
			videos = new video5Track(rutaBase+"flv/balance.flv", personas[0], personas[1], personas[2], personas[3], personas[4], bocas[0], bocas[1], bocas[2], bocas[3], n1, n2, n3, n4);
			
			addChild(videos);
			videos.playVideo();
			videos.x = -100;
			videos.y = -10;
			videos.scaleX = videos.scaleY = .7;
			}
			else if(videoString == "clase")
			{
				videoc = new video2Trackc(rutaBase+"flv/clase.flv", personas[0], personas[1], bocas[0], bocas[1], n1, n2);
				addChild(videoc);
				videoc.playVideo();
				videoc.x = -100;
				videoc.y = -10;
				videoc.scaleX = videoc.scaleY = .7;
			}
			else if(videoString == "practicante")
			{
				videop = new video2Trackp(rutaBase+"flv/practicante.flv", personas[0], personas[1], bocas[0], bocas[1], n1, n2);
				addChild(videop);
				videop.playVideo();
				videop.x = -100;
				videop.y = -10;
				videop.scaleX = videop.scaleY = .7;
			}
			
		}
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
			if(videoString == "balance")
			{
			videos.stopVideo();
			}
			else if(videoString == "clase")
			{
			videoc.stopVideo();
			}
			else if(videoString == "practicante")
			{
			videop.stopVideo();
			}
			
		}
		
		function init() {
			if(!yaseHizo)
			{
				totalCaras();
				cargaVideo();}
			else {
				listoBtn.addEventListener(MouseEvent.CLICK, repiteMovie);
				if(videoString == "balance")
				videos.playVideo();
				else if(videoString == "clase")
				videoc.playVideo();
				else if(videoString == "practicante")
				videop.playVideo();
			}
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut});
		}
		
		function quitaMovie(i:Number):void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente, onCompleteParams:[i]});
		}
		
		function initSiguiente(i:Number):void {
			var padre = MovieClip(parent);
			visible = false;
			if(i == 1) {
				padre.statusMc.posFlecha(4);
				padre.enviarAmigo.init();
			}
			else if( i==2) {
				if(videoString == "balance")
				padre.elige4Caras.init();
				else 
				padre.eligeCara.init();
			}
			else{
				padre.confirmacionMc.init();
			}
		}
	}
}