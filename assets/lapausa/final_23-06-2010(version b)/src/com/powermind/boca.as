﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	
	public class boca extends MovieClip {
		
		public var maskMc:MovieClip = new MovieClip();
		public var bordeMc:MovieClip = new MovieClip();
		
	public function boca() {
		
		moldeBoca();
		p1.buttonMode = p2.buttonMode = p3.buttonMode = p4.buttonMode = a1.buttonMode = a2.buttonMode = true;
		
		p1.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p1.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p2.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p2.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p3.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p3.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		p4.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		p4.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a1.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a1.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
		
		a2.addEventListener(MouseEvent.MOUSE_DOWN, dragPunto);
		a2.addEventListener(MouseEvent.MOUSE_UP, stopDragPunto);
	
		addChildAt(maskMc,1);
		addChildAt(bordeMc,0);
	
		dibujaMask();
		dibujaBorde();
			
	}
	
	function dontShow() {
		p1.alpha = p2.alpha = p3.alpha = p4.alpha = a1.alpha = a2.alpha = 0;
		bordeMc.alpha = 0;
	}
	
	function Show() {
		p1.alpha = p2.alpha = p3.alpha = p4.alpha = a1.alpha = a2.alpha = 1;
		bordeMc.alpha = 1;
		moldeBoca();
		dibujaMask();
        dragPosicion(null);
	}
	
	private function dragPunto(event:MouseEvent){ 
		addEventListener(Event.ENTER_FRAME,  dragPosicion);
		event.target.startDrag();
	}
	
	private function dragPosicion(event:Event){
		maskMc.graphics.clear();
		bordeMc.graphics.clear();
		dibujaMask();
		dibujaBorde();
	}
	private function moldeBoca() {
		p1.x= -25;
		p1.y= -35;
		p2.x= 25;
		p2.y= -35;
		p3.x= 20;
		p3.y= 0;
		p4.x= -20;
		p4.y= 0;
		a1.x= 0;
		a1.y= -35;
		a2.x= 0;
		a2.y= 10;
	}
	
	
	private function stopDragPunto(event:MouseEvent){ 
		removeEventListener(Event.ENTER_FRAME,  dragPosicion);
		event.target.stopDrag();
	}
	
	private function dibujaMask(){
	
	maskMc.graphics.lineStyle(1, 0x000000, .5);
	maskMc.graphics.beginFill( 0x009900, .5);
	maskMc.graphics.moveTo(p1.x, p1.y);
	maskMc.graphics.curveTo(a1.x, a1.y, p2.x, p2.y);
	maskMc.graphics.lineTo(p3.x, p3.y);
	maskMc.graphics.curveTo(a2.x, a2.y, p4.x, p4.y);
	maskMc.graphics.lineTo(p1.x, p1.y);
	maskMc.graphics.endFill();

	}
	
	private function dibujaBorde(){ 
	
	bordeMc.graphics.lineStyle(3, 0xFF0000, 1);
	bordeMc.graphics.moveTo(p1.x, p1.y);
	bordeMc.graphics.curveTo(a1.x, a1.y, p2.x, p2.y);
	bordeMc.graphics.lineTo(p3.x, p3.y);
	bordeMc.graphics.curveTo(a2.x, a2.y, p4.x, p4.y);
	bordeMc.graphics.lineTo(p1.x, p1.y);
	bordeMc.graphics.endFill();
	}
		
	}
	
}