﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	import flash.external.ExternalInterface;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	import joe.*;
	
	import com.adobe.images.*;

	public class enviarAmigo extends MovieClip{
			
			var video:Number;
			var mcCount:int=5;
			var nextY:int=0;
			var i:int=0;
			var max:int=5;
			var holder:MovieClip = new MovieClip();
			public var videos:Video;
			var codificador:PNGEncoder;
			var Hash:String = "noexiste";
			var enviado:Boolean = false;
			var errorBox:ErrorBox;
			var validador:ValidarRut = new ValidarRut();
			
			public var p1:Personaje;
			public var p2:Personaje;
			public var p3:Personaje;
			public var p4:Personaje;
			public var p5:Personaje;
			
		function enviarAmigo():void{
			
			addChild(holder);
			for (i=0; i<mcCount; i++) {
				var barMC:mc_campo_correo = new mc_campo_correo();
				barMC.y=nextY;
				holder.addChild(barMC);
				nextY+=barMC.height+1;
			}
			scrollComp.source=holder;
			
			agregarBtn.addEventListener(MouseEvent.CLICK, agregar);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
			enviarBtn.addEventListener(MouseEvent.CLICK, enviar);
		}
		
		function iniciaTextos() {
			nombreTxt.text = "";
			emailTxt.text = "";
			mensajeTxt.text = "";
			rutTxt.text = "";
			dvTxt.text = "";
			nombreTxt.tabIndex = 0;
			emailTxt.tabIndex = 3;
			mensajeTxt.tabIndex = 4;
			rutTxt.tabIndex = 1;
			dvTxt.tabIndex = 2;
			
		}
		function valida():Boolean {
			
			if(nombreTxt.text == "") {
				errorBox = new ErrorBox("Error en el Nombre", "Debe ingresar un nombre. Intente nuevamente.");
				addChild(errorBox);
				return false;
			}
			else if(emailTxt.text == "") {
				errorBox = new ErrorBox("Error en el Email", "Debe ingresar su email para continuar.");
				addChild(errorBox);
				return false;
			}
			else if(!(validador.validar(rutTxt.text+"-"+dvTxt.text))) {
				errorBox = new ErrorBox("Error en el Rut", "Debe ingresar un rut válido para continuar.");
				addChild(errorBox);
				return false;
			}			
			else {
				trace("entre");
				 return true;
			}
		}
				
		function enviar(e:MouseEvent) {
			if(valida() && !enviado){
			cargaEnvio();
			this.videos.addEventListener(Video.VIDEO_SUBIDO,videoSubido);
			}
			else if(valida() && enviado)
			{
				var m:Mail = new Mail();
				m.setCreador(nombreTxt.text, emailTxt.text, mensajeTxt.text, Hash, video);
				var za:mc_campo_correo = this.holder.getChildAt(0) as mc_campo_correo;
				if(za.emailTxt.text !=""){
				for(var i:int=0; i<this.holder.numChildren-1; i++) {
					var d:mc_campo_correo = this.holder.getChildAt(i) as mc_campo_correo;
						if(d.emailTxt.text != "")
							m.agregarInvitado(d.emailTxt.text);
					}
				quitaMovie();
			m.commit();
			enviarBtn.removeEventListener(MouseEvent.CLICK, enviar);
				}
				else {
					errorBox = new ErrorBox("Error en email de Amigo", "Debe ingresar al menos un email de un amigo para continuar.");
					addChild(errorBox);
				}
			}
			}
			
			function videoSubido(e:Event) {
			enviado = true;
			trace("entro a videosubido interno");
			if(Hash =="noexiste")
				Hash= this.videos.hash;
			var m:Mail = new Mail();
			m.setCreador(nombreTxt.text, emailTxt.text, mensajeTxt.text, Hash, video);
			var za:mc_campo_correo = this.holder.getChildAt(0) as mc_campo_correo;
				if(za.emailTxt.text !=""){
				for(var i:int=0; i<this.holder.numChildren-1; i++) {
					var d:mc_campo_correo = this.holder.getChildAt(i) as mc_campo_correo;
						if(d.emailTxt.text != "")
							m.agregarInvitado(d.emailTxt.text);
					}
					
			m.commit();
			enviarBtn.removeEventListener(MouseEvent.CLICK, enviar);
			quitaMovie();
				}
				else {
					errorBox = new ErrorBox("Error en email de Amigo", "Debe ingresar al menos un email de un amigo para continuar.");
					addChild(errorBox);
				}
			}

		function cargaEnvio() {
			if(this.video == 1){
			p1 = MovieClip(parent).eligeCara.caraUno;
			p2 = MovieClip(parent).eligeCara.caraDos;
			
			this.videos = new Video(1);
			
			this.videos.agregarPersona(PNGEncoder.encode(p1.bmd), PNGEncoder.encode(p1.bmdb), p1.nombre, 1);
			this.videos.agregarPersona(PNGEncoder.encode(p2.bmd), PNGEncoder.encode(p2.bmdb), p2.nombre, 2);
			this.videos.setCreador(nombreTxt.text, emailTxt.text, mensajeTxt.text, rutTxt.text+"-"+dvTxt.text);
			this.videos.commit();
			}
			else if(this.video ==2) {
			p1 = MovieClip(parent).elige4Caras.caraDos;
			p2 = MovieClip(parent).elige4Caras.caraUno;
			p3 = MovieClip(parent).elige4Caras.caraTres;
			p4 = MovieClip(parent).elige4Caras.caraCuatro;
			p5 = MovieClip(parent).elige4Caras.caraCinco;
			
			this.videos = new Video(2);
			
			this.videos.agregarPersona(PNGEncoder.encode(p1.bmd), PNGEncoder.encode(p1.bmdb), p1.nombre, 1);
			this.videos.agregarPersona(PNGEncoder.encode(p2.bmd), PNGEncoder.encode(p2.bmdb), p2.nombre, 2);
			this.videos.agregarPersona(PNGEncoder.encode(p3.bmd), PNGEncoder.encode(p3.bmdb), p3.nombre, 3);
			this.videos.agregarPersona(PNGEncoder.encode(p4.bmd), PNGEncoder.encode(p4.bmdb), p4.nombre, 4);
			this.videos.agregarPersona(PNGEncoder.encode(p5.bmd), PNGEncoder.encode(p5.bmdb), p5.nombre, 5);
			this.videos.setCreador(nombreTxt.text, emailTxt.text, mensajeTxt.text, rutTxt.text+"-"+dvTxt.text);
			this.videos.commit();
			}
			if(this.video == 3){
			p1 = MovieClip(parent).eligeCara.caraUno;
			p2 = MovieClip(parent).eligeCara.caraDos;
			
			this.videos = new Video(3);
			
			this.videos.agregarPersona(PNGEncoder.encode(p1.bmd), PNGEncoder.encode(p1.bmdb), p1.nombre, 1);
			this.videos.agregarPersona(PNGEncoder.encode(p2.bmd), PNGEncoder.encode(p2.bmdb), p2.nombre, 2);
			this.videos.setCreador(nombreTxt.text, emailTxt.text, mensajeTxt.text, rutTxt.text+"-"+dvTxt.text);
			this.videos.commit();
			}
			
		}
		
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
		}
		
		function agregar(event:MouseEvent):void {
			if (max<10) {
				var barMC:mc_campo_correo = new mc_campo_correo();
				barMC.y=nextY;
				holder.addChild(barMC);
				nextY+=barMC.height+1;
				scrollComp.source=holder;
				max++;
			}
		}
		
		function init() {
			iniciaTextos();
			MovieClip(parent).statusMc.posFlecha(4);
			visible = true;
			TweenMax.to(this, 1, {autoAlpha:1, ease:Expo.easeOut});
		}
		
		function quitaMovie():void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente});
		}
		
		function initSiguiente():void {
			var padre = MovieClip(parent);
			visible = false;
			padre.confirmacionMc.hash = this.Hash;
			padre.confirmacionMc.init();
		}
	}
}