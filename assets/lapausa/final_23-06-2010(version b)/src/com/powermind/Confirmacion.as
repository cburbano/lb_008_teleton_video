﻿package com.powermind{
	import flash.display.*;
	import flash.events.*;
	import flash.net.*;
	import flash.utils.escapeMultiByte;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;

	public class Confirmacion extends MovieClip{
		public var hash:String;
		
		function Confirmacion():void{
			visible = false;
			alpha = 0;
			twitterBtn.addEventListener(MouseEvent.CLICK, twitter);
			facebookBtn.addEventListener(MouseEvent.CLICK, facebook);
			
			twitterBtn.buttonMode = true;
			facebookBtn.buttonMode = true;
			
			verBtn.addEventListener(MouseEvent.CLICK, verVideo);
			cerrarBtn.addEventListener(MouseEvent.CLICK, cierraFunc);
		}
		
		function verVideo(e:MouseEvent) {
			quitaMovie(2);
			verBtn.removeEventListener(MouseEvent.CLICK, verVideo);
		}
		
		function cierraFunc(e:MouseEvent) {
			MovieClip(parent).closeAll();
		}
		
		
		function twitter(e:MouseEvent) {
			var s:String= 'http://twitter.com/home?status='+escapeMultiByte('Mira esta pausita que me estoy tomando http://www.lapausa.cl/videos/ver/'+this.hash);
			navigateToURL(new URLRequest(s), "_blank");
		}
		
		function facebook(e:MouseEvent) {
			var s:String= 'http://www.facebook.com/sharer.php?u='+escapeMultiByte('http://www.lapausa.cl/videos/ver/'+this.hash+'&t=Mira la pausa que me estoy tomando');
			navigateToURL(new URLRequest(s), "_blank");
		}
		
		function init() {
			verBtn.addEventListener(MouseEvent.CLICK, verVideo);
			visible = true;
			TweenMax.to(this, 1.5, {autoAlpha:1, ease:Expo.easeOut});
			this.hashText.text = "http://localhost/lapausa/videos/ver/"+this.hash;
		}
		
		function quitaMovie(i:Number):void {
			TweenMax.to(this, 1, {autoAlpha:0, ease:Expo.easeOut, onComplete:initSiguiente, onCompleteParams:[i]});
		}
		
		function initSiguiente(i:Number):void {
			var padre = MovieClip(parent);
			visible = false;
			if(i == 1)
			padre.enviarAmigo.init();
			else if(i ==2)
			padre.prevVideo.init();
			}
	}
}