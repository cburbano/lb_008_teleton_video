﻿package com.powermind
{
   import flash.display.DisplayObject;
   import flash.display.*;
   public class Tools
   {
      public static function duplicateMovieClip(target:MovieClip):MovieClip
      {
         var targetClass:Class;
         targetClass = Object(target).constructor;
         var duplicado:MovieClip = new targetClass();

         duplicado.transform = target.transform;
         duplicado.filters = target.filters;
         duplicado.cacheAsBitmap = target.cacheAsBitmap;
         duplicado.opaqueBackground = target.opaqueBackground;
		 duplicado.graphics = target.graphics;

         target.parent.addChild(duplicado);
         return duplicado;
      }
   }
}