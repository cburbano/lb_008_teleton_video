﻿package cl.burbano{

	import flash.display.Sprite;
	import flash.display.SimpleButton;
	import flash.filters.GlowFilter;
	import flash.filters.BlurFilter;
	import flash.filters.DropShadowFilter;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.net.*;
	import flash.display.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	import com.powermind.*;

	
	public class Main extends MovieClip{
		//class definition
		
		//properties
		private var fondo:FondoAsset = new FondoAsset();
		private var maggiBtn:MaggiAsset = new MaggiAsset();
		private var facebookBtn:FacebookAsset= new FacebookAsset();
		private var titulo:TituloAsset = new TituloAsset ();
		private var logo:LogoAsset = new LogoAsset();
		private var sopas:SopasAsset = new SopasAsset();
		private var escena:Escena = new Escena();
		private var aplicacion:menuLateral;
		private var escenarios:Array = new Array('arte', 'balances', 'practicante');
		private var referencias:Array = new Array();
		private var posX:Array = new Array();
		private var posY:Array = new Array();
		private var margenX:Array = new Array(); //valores en porcentaje
		private var margenY:Array = new Array(); //valores en porcentaje
		private var stgIniW:Number=1000;
		private var stgIniH:Number=574;
		private var diferenciaX:Number;
		private var diferenciaY:Number;
		private var seccionPrevia:int=-1;
		private var videoPlay:Boolean;
		
		private var escalaListener:Object = new Object();
		
		public function Main() {
			this.addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);

		}
		private function handleAddedToStage(event:Event):void
		{
			dibujare();
			resetearPosiciones();
			this.agregarListeners();
			moverAlEscalar(true);
			this.animarInicio();
		}
		
		public function dibujare():void {

			referencias.push(this.fondo);
			posX.push(0);
			posY.push(0);
			margenX.push(0);
			margenY.push(0);
			referencias.push(this.maggiBtn);
			posX.push(945);
			posY.push(521);
			margenX.push(0.6);
			margenY.push(0.6);
			referencias.push(this.logo);
			posX.push(0);
			posY.push(0);
			margenX.push(-0.6);
			margenY.push(-0.6);
			referencias.push(this.titulo);
			posX.push(232);
			posY.push(23);
			margenX.push(0);
			margenY.push(-0.4);
			referencias.push(this.escena);
			posX.push(-11);
			posY.push(216);
			margenX.push(0);
			margenY.push(0);
			referencias.push(this.sopas);
			posX.push(93);
			posY.push(502);
			margenX.push(-0.6);
			margenY.push(0.6);
			referencias.push(this.facebookBtn);
			posX.push(740);
			posY.push(70);
			margenX.push(0);
			margenY.push(-0.4);

			//activar botones de videos
			this.escena.seccionEscogida = 0;
			this.escena.arteBtn.seccion = 0;
			this.escena.balancesBtn.seccion = 1;
			this.escena.practicanteBtn.seccion = 2;
					
		}
		
		public function cierraFuncion() {
			this.removeChild(aplicacion);
			aplicacion = new menuLateral();
			this.addChild(aplicacion);
			this.aplicacion.alpha = 0;
			this.aplicacion.x = 840
			this.aplicacion.y = 60
			traerAlFrente(this.aplicacion);
			TweenMax.to(this.aplicacion, 2, {delay:0, alpha:1, ease:Strong.easeInOut});
		}
	
		private function resetearPosiciones():void {
			
			for (var i:int=0; i<this.referencias.length; i++){
				this.referencias[i].posX = this.referencias[i].x;
				this.referencias[i].posY = this.referencias[i].y;
				this.referencias[i].alpha = 0;
				this.referencias[i].x = this.posX[i];
				this.referencias[i].y = this.posY[i];
				this.addChild(this.referencias[i]);
			}
			aplicacion = new menuLateral();
			this.addChild(aplicacion);
			this.aplicacion.alpha = 0;
			this.aplicacion.x = 840
			this.aplicacion.y = 60
			traerAlFrente(this.aplicacion);
			
		}
		
		function traerAlFrente(MC:MovieClip):void{ 
		   //Esta variable obtiene la posición (profundidad) del MovieClip 
		   var pos:Number = this.getChildIndex(MC) 
		   //Mientras que el clip no se encuentre por encima 
		   //del resto, se ejecutará la siguiente función 
		   while(pos < this.numChildren - 1){ 
			  //La variable "siguiente" obtiene el 
			  //MovieClip inmediatamente superior 
			  var siguiente:MovieClip = this.getChildAt(pos+1) as MovieClip 
			  //Se intercambia la posición de los dos clips 
			  this.swapChildren(MC,siguiente) 
			  //Actualizamos el valor de la variable "pos" 
			  pos = this.getChildIndex(MC) 
		   } 
		}
				
		public function animarInicio(){
			var so:Sonido = new Sonido("/mp3/fondo.mp3", 10, 0, 1);
			
			TweenMax.to(this.fondo, 0.8, {alpha:1, ease:Strong.easeInOut});			
			this.maggiBtn.rotation -= 50; this.maggiBtn.x += 80;
			TweenMax.to(this.maggiBtn, 0.8, {rotation:this.maggiBtn.rotation + 50, x:this.maggiBtn.x - 80 , ease:Strong.easeInOut, alpha: 1, delay:0.5 });			
			this.facebookBtn.y += 100; 
			TweenMax.to(this.facebookBtn, 1, {alpha:1, y:this.facebookBtn.y-100, ease:Elastic.easeOut, delay:0.6 });
			this.logo.y = -800;
			TweenMax.to(this.logo, 1.5, {alpha: 1, y:this.logo.y+800, ease:Back.easeOut, delay:0.5 });
			TweenMax.to(this.titulo, 1, {ease:Strong.easeInOut, alpha: 1, delay:0 });
			this.sopas.x -= 300;
			TweenMax.to(this.sopas, 0.8, {alpha: 1, x:this.sopas.x+300, ease:Back.easeOut});
			TweenMax.to(this.escena, 2, {delay:0, alpha:1, ease:Strong.easeInOut, onComplete:test});
			TweenMax.to(this.aplicacion, 2, {delay:0, alpha:1, ease:Strong.easeInOut});
			
		}
		
		private function test(){
			
			this.escena["animacion_"+escenarios[1]].gotoAndStop(1);
			this.escena["animacion_"+escenarios[1]].alpha = 1;
			this.escena["animacion_"+escenarios[1]].play();
			this.seccionPrevia = 1;
			
			for (var i:Number = 0; i < 3; i++) {
				this.escena["animacion_"+escenarios[i]].videoPlay = false;
				this.escena["video_"+escenarios[i]].videoPlay = false;
			}
			
		}
		
		private function onClick(event:MouseEvent):void { //videos
			
			if (this.seccionPrevia != -1){
				TweenMax.to(event.currentTarget, 1, {glowFilter:{color:0xBD2702, alpha:1, blurX:15, blurY:15}});
	
				for (var i:Number = 0; i < 3; i++) {
					
					if (this.seccionPrevia != event.currentTarget.seccion){
					
						//primero limpiamos el video en curso
						if (this.seccionPrevia == i){
							
							this.escena["video_"+escenarios[this.seccionPrevia]].stop();
							this.escena["animacion_"+escenarios[this.seccionPrevia]].stop();
							
							if (this.escena["video_"+escenarios[this.seccionPrevia]].miPlayer){
								this.escena["video_"+escenarios[this.seccionPrevia]].miPlayer.stop();
							}
							
							TweenMax.to(this.escena["animacion_"+escenarios[this.seccionPrevia]], 0.5, {alpha:0, onComplete:desaparecer, onCompleteParams:[this.escena["animacion_"+escenarios[this.seccionPrevia]]]});
							TweenMax.to(this.escena["video_"+escenarios[this.seccionPrevia]], 0.7, {alpha:0, onComplete:desaparecer, onCompleteParams:[this.escena["video_"+escenarios[this.seccionPrevia]]]});
							
						}
		
						
						if (event.currentTarget.seccion == i){
							
							this.escena["animacion_"+escenarios[event.currentTarget.seccion]].gotoAndStop(1);
							this.escena["animacion_"+escenarios[event.currentTarget.seccion]].alpha = 1;
							this.escena["animacion_"+escenarios[event.currentTarget.seccion]].visible = true;
							this.escena["animacion_"+escenarios[event.currentTarget.seccion]].play();
							this.escena["video_"+escenarios[event.currentTarget.seccion]].visible = true;
		
						}
					}
									 
				} 
				this.seccionPrevia = event.currentTarget.seccion;
			}

		}
		
		private function desaparecer(_m:MovieClip):void{
			_m.visible = false;
			_m.gotoAndStop(1);
		}
		
		private function onClickVideo(event:MouseEvent):void {
			event.currentTarget.parent.gotoAndPlay('f_entraVideo');
		}
		
		public function videoCambiaryAndar(_n:int):void {
			this.escena["video_"+escenarios[_n]].gotoAndPlay(2);
			this.escena["video_"+escenarios[_n]].visible = true;
			this.escena["video_"+escenarios[_n]].alpha = 1;
			
		}
		
		public function videoDesCambiaryAndar(_n:int):void {
			this.escena["animacion_"+escenarios[_n]].gotoAndPlay('f_saleVideo');
		}
		
		public function onCerrarVideo(event:MouseEvent):void {
			event.currentTarget.parent.miPlayer.stop();
			event.currentTarget.parent.gotoAndPlay('f_salir');
		}
 
		
		private function onRollover(event:MouseEvent):void {
			TweenMax.to(event.currentTarget, 1, {glowFilter:{color:0xA36F03, alpha:1, blurX:15, blurY:15}});
		}
		
		private function onRollout(event:MouseEvent):void {
			TweenMax.to(event.currentTarget, 1, {glowFilter:{color:0xA36F03, alpha:0, blurX:30, blurY:30}});
		}
		
		private function onClickFace(event:MouseEvent):void {
			var targetURL:URLRequest = new URLRequest('http://www.facebook.com/share.php?u=http://www.burbanoapp.cl/lapausa.cl/staging/index2.php');
			
			try {
				navigateToURL(targetURL, '_blank');
			} catch (e:Error) {
				trace("Error de conexión con Facebook");
			}

		}
		
		private function onRolloverFace(event:MouseEvent):void {
			this.facebookBtn.gotoAndPlay('f_over');
		}
		
		private function onRolloutFace(event:MouseEvent):void {
			this.facebookBtn.gotoAndPlay('f_normal');
		}
		
		private function onRolloverMaggi(event:MouseEvent):void {
			this.maggiBtn.gotoAndPlay('f_over');
		}
		
		private function onRolloutMaggi(event:MouseEvent):void {
			this.maggiBtn.gotoAndPlay('f_normal');
		}
		
		private function onClickMaggi(event:MouseEvent):void {
			var targetURL:URLRequest = new URLRequest('http://www.maggi.cl');
			
			try {
				navigateToURL(targetURL, '_blank');
			} catch (e:Error) {
				trace("Error de conexión con Maggi");
			}
		}
		
		private function onClickSopas(event:MouseEvent):void {
			var targetURL:URLRequest = new URLRequest('http://www.maggi.cl/despensa/despensa_sopa_para_uno.aspx?ContentID=22&SID=21');
			
			try {
				navigateToURL(targetURL, '_blank');
			} catch (e:Error) {
				trace("Error de conexión con la Despensa Maggi");
			}

		}
		
		private function agregarListeners():void{
			stage.addEventListener(Event.RESIZE, resizeListener);
			
			this.escena.animacion_arte.videoBtn.addEventListener(MouseEvent.CLICK, onClickVideo);
			this.escena.animacion_balances.videoBtn.addEventListener(MouseEvent.CLICK, onClickVideo);
			this.escena.animacion_practicante.videoBtn.addEventListener(MouseEvent.CLICK, onClickVideo);
			
			this.escena.video_arte.cerrarBtn.addEventListener(MouseEvent.CLICK, onCerrarVideo);
			this.escena.video_balances.cerrarBtn.addEventListener(MouseEvent.CLICK, onCerrarVideo);
			this.escena.video_practicante.cerrarBtn.addEventListener(MouseEvent.CLICK, onCerrarVideo);
			
			this.escena.arteBtn.addEventListener(MouseEvent.CLICK, onClick);
			this.escena.balancesBtn.addEventListener(MouseEvent.CLICK, onClick);
			this.escena.practicanteBtn.addEventListener(MouseEvent.CLICK, onClick);
			
			this.escena.arteBtn.addEventListener(MouseEvent.MOUSE_OVER, onRollover);
			this.escena.balancesBtn.addEventListener(MouseEvent.MOUSE_OVER, onRollover);
			this.escena.practicanteBtn.addEventListener(MouseEvent.MOUSE_OVER, onRollover);
			
			this.escena.arteBtn.addEventListener(MouseEvent.MOUSE_OUT, onRollout);
			this.escena.balancesBtn.addEventListener(MouseEvent.MOUSE_OUT, onRollout);
			this.escena.practicanteBtn.addEventListener(MouseEvent.MOUSE_OUT, onRollout);
			
			//activar video Facebook
			//this.facebookBtn.Btn.addEventListener(MouseEvent.CLICK, onClickFace);
			//this.facebookBtn.Btn.addEventListener(MouseEvent.MOUSE_OVER, onRolloverFace);
			//this.facebookBtn.Btn.addEventListener(MouseEvent.MOUSE_OUT, onRolloutFace);
			
			this.maggiBtn.Btn.addEventListener(MouseEvent.CLICK, onClickMaggi);
			this.maggiBtn.Btn.addEventListener(MouseEvent.MOUSE_OVER, onRolloverMaggi);
			this.maggiBtn.Btn.addEventListener(MouseEvent.MOUSE_OUT, onRolloutMaggi);
			
			this.sopas.addEventListener(MouseEvent.CLICK, onClickSopas);
		}
		
		function resizeListener (e:Event):void {
			moverAlEscalar();
		}
		
		function moverAlEscalar(_inicio:Boolean=false):void {
			diferenciaX = (this.stage.stageWidth - stgIniW)/2;
			diferenciaY = (this.stage.stageHeight - stgIniH)/2;
			
			//fondo
			this.referencias[0].x = -diferenciaX;
			this.referencias[0].y = -diferenciaY;
			this.referencias[0].width = this.stage.stageWidth;
			this.referencias[0].height = this.stage.stageHeight;
			
			for (var i:int=1; i<this.referencias.length; i++){
				
				if (_inicio){//posicionar antes de que se escale, en caso de que parta grande
					
					if (diferenciaX > 0 ){
						this.referencias[i].x = this.posX[i] + (this.margenX[i] * diferenciaX);
					}
					
					if (diferenciaY > 0 ){
						this.referencias[i].y = this.posY[i] + (this.margenY[i] * diferenciaY);
					}
					
				} else {
					
					var ef:Object = {
						x: this.posX[i] + (this.margenX[i] * diferenciaX),
						y: this.posY[i] + (this.margenY[i] * diferenciaY),
						delay:0,
						transition: Strong.easeInOut
					}
					TweenMax.to(this.referencias[i], 1, ef);
					
				}
				
			}
		}
		
	}
	
}
