﻿package cl.burbano{

	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.events.*;
	import flash.net.URLRequest;
	import flash.display.*;

	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	import cl.burbano.Main;
	
	public class Loading extends Sprite{
		// class definition
		
		//properties
		//private var url:String = "pagina1.swf";
		public var loading:LoadingAsset= new LoadingAsset();
		private var activado:int=1;
		private var porcentaje:Number=0;
		private var movieRef:MovieClip;
		private var videosOut:int=0;
		
		public var loader:Loader = new Loader();
		

		public function Loading() {
			stage.scaleMode = StageScaleMode.NO_SCALE;
			resetear(loading, 598, 294, 0);
			this.addChild(loading);
			
			TweenMax.to(this.loading, 1, {alpha:1, ease:Strong.easeInOut, alpha: 1, delay:0 });	
			cargarPagina("/swf/mainMovie.swf");
		
		}
		
	
		private function resetear(_m:Sprite, _X:Number, _Y:Number, _alpha:Number=0 ):void {
			_m.x = _X;
			_m.y = _Y;
			_m.alpha = _alpha;
		}
	
		public function cargarPagina(__url:String) {
			//Creo el objeto cargador
			this.loader = new Loader();
			//Le añado los listeners
			ponerListeners(loader.contentLoaderInfo);
			//Creo el objeto que contendrá la petición
			var peticion:URLRequest = new URLRequest(__url);
			//Cargo la petición
			loader.load(peticion);		
			
		}
 
		//Añade los listeners al cargador
		function ponerListeners(dispatcher:IEventDispatcher):void {
			dispatcher.addEventListener(Event.COMPLETE, completeHandler);
			dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			dispatcher.addEventListener(Event.INIT, initHandler);
			dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			dispatcher.addEventListener(Event.OPEN, openHandler);
			dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
		}
 
		//Funcion que se ejecuta cuando termina la descarga. 
		function completeHandler(event:Event):void {

			//this.addChild(event.target.content);
			this.movieRef = event.target.content;
			//addChild(MovieClip(movieRef));
			this.sacarSopas();
			
		}
		 
		//Se ejecuta al recibir un código de estado HTTP
		function httpStatusHandler(event:HTTPStatusEvent):void {
			//trace("httpStatusHandler: " + event);
		}
		 
		//Se distribuye cuando las propiedades del objeto cargado están disponibles
		//A partir de su distribucion podriamos modificar el objeto (ancho, escala, etc)
		function initHandler(event:Event):void {
			//trace("initHandler: " + event);
		}
		 
		//Se ejecuta cuando hay un error de entrada/salida, por ejemplo, al intentar cargar un 
		//archivo inexistente
		function ioErrorHandler(event:IOErrorEvent):void {
			//trace("ioErrorHandler: " + event);
		}
		 
		//Se ejecuta cuando comienza la operación de carga
		function openHandler(event:Event):void {
			//trace("openHandler: " + event);
		}
		 
		//Se ejecuta periodicamente mientras dura la carga
		function progressHandler(event:ProgressEvent):void {
			
			if (event.bytesLoaded*100/event.bytesTotal/20 > this.activado-1){
				
				TweenMax.to(this.loading['sopa'+this.activado], 0.7, {alpha:1, ease:Strong.easeIn, alpha: 1, delay:0.1*(this.activado) });		
				this.activado++;
				
			}
			
		}
		
		function sacarSopas(){
			
			for (var i:Number = 1; i <= 5; i++) {
    			TweenMax.to(this.loading['sopa'+i], 0.7, {alpha:0, y:this.loading['sopa'+i].y-100, ease:Back.easeIn, alpha: 1, delay:0.7+0.1*(i),onComplete:sacarLoading});
			}
			TweenMax.to(this.loading.logoAsset, 1.5, {alpha:0, y:1500, ease:Back.easeIn, delay:1,onComplete:sacarLoading});
			
		}
		
		function sacarLoading(){
			
			videosOut++
			if (videosOut == 6){
				addChild(this.movieRef);
			}
			
		}
		
	}
	
}
