﻿package cl.burbano{

	import flash.display.Sprite;
	import flash.display.SimpleButton;
	import flash.filters.GlowFilter;
	import flash.filters.BlurFilter;
	import flash.filters.DropShadowFilter;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.net.*;
	import flash.display.*;
	
	import gs.*;
	import gs.easing.*;
	import gs.plugins.*;
	
	import com.powermind.menuLateral;

	
	public class Main extends MovieClip{
		//class definition
		
		//properties
		private var fondo:FondoAsset = new FondoAsset();
		private var maggiBtn:MaggiAsset = new MaggiAsset();
		private var facebookBtn:FacebookAsset= new FacebookAsset();
		private var titulo:TituloAsset = new TituloAsset ();
		private var logo:LogoAsset = new LogoAsset();
		private var sopas:SopasAsset = new SopasAsset();
		private var escena:Escena = new Escena();
		private var escenarios:Array = new Array('arte', 'balances', 'practicante');
		public var variable:String='holanda';
		private var referencias:Array = new Array();
		private var posX:Array = new Array();
		private var posY:Array = new Array();
		private var margenX:Array = new Array(); //valores en porcenaje
		private var margenY:Array = new Array(); //valores en porcentaje
		private var stgIniW:Number=1000;
		private var stgIniH:Number=574;
		private var diferenciaX:Number;
		private var diferenciaY:Number;
		private var seccionPrevia:int=-1;
		
		private var escalaListener:Object = new Object();
		

		
		public function Main() {
			
			this.dibujar();
			resetearPosiciones();
			moverAlEscalar(true);
			this.agregarListeners();
			this.animarInicio();

		}
		
		private function dibujar():void {

			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			this.referencias.push(this.fondo);
			this.posX.push(0);
			this.posY.push(0);
			this.margenX.push(0);
			this.margenY.push(0);
			this.referencias.push(this.maggiBtn);
			this.posX.push(945);
			this.posY.push(521);
			this.margenX.push(0.6);
			this.margenY.push(0.6);
			this.referencias.push(this.logo);
			this.posX.push(0);
			this.posY.push(0);
			this.margenX.push(-0.6);
			this.margenY.push(-0.6);
			this.referencias.push(this.titulo);
			this.posX.push(232);
			this.posY.push(23);
			this.margenX.push(0);
			this.margenY.push(-0.4);
			this.referencias.push(this.escena);
			this.posX.push(-11);
			this.posY.push(216);
			this.margenX.push(0);
			this.margenY.push(0);
			this.referencias.push(this.sopas);
			this.posX.push(93);
			this.posY.push(502);
			this.margenX.push(-0.6);
			this.margenY.push(0.6);
			this.referencias.push(this.facebookBtn);
			this.posX.push(834);
			this.posY.push(530);
			this.margenX.push(0.5);
			this.margenY.push(0.6);

			//activar botones de videos
			this.escena.seccionEscogida = 0;
			this.escena.arteBtn.seccion = 0;
			this.escena.balancesBtn.seccion = 1;
			this.escena.practicanteBtn.seccion = 2;
					
		}
	
		
		private function resetearPosiciones():void {
			
			for (var i:int=0; i<this.referencias.length; i++){
				this.referencias[i].posX = this.referencias[i].x;
				this.referencias[i].posY = this.referencias[i].y;
				this.referencias[i].alpha = 0;
				this.referencias[i].x = this.posX[i];
				this.referencias[i].y = this.posY[i];
				this.addChild(this.referencias[i]);
			}
			this.menuLateral.alpha = 0;
			this.menuLateral.x = 840
			this.menuLateral.y = 60
			traerAlFrente(this.menuLateral);
			
		}
		
		function traerAlFrente(MC:MovieClip):void{ 
		   //Esta variable obtiene la posición (profundidad) del MovieClip 
		   var pos:Number = this.getChildIndex(MC) 
		   //Mientras que el clip no se encuentre por encima 
		   //del resto, se ejecutará la siguiente función 
		   while(pos < this.numChildren - 1){ 
			  //La variable "siguiente" obtiene el 
			  //MovieClip inmediatamente superior 
			  var siguiente:MovieClip = this.getChildAt(pos+1) as MovieClip 
			  //Se intercambia la posición de los dos clips 
			  this.swapChildren(MC,siguiente) 
			  //Actualizamos el valor de la variable "pos" 
			  pos = this.getChildIndex(MC) 
		   } 
		}
				
		public function animarInicio(){
			
			TweenMax.to(this.fondo, 0.8, {alpha:1, ease:Strong.easeInOut});			
			this.maggiBtn.rotation -= 50; this.maggiBtn.x += 80;
			TweenMax.to(this.maggiBtn, 0.8, {rotation:this.maggiBtn.rotation + 50, x:this.maggiBtn.x - 80 , ease:Strong.easeInOut, alpha: 1, delay:0.5 });			
			this.facebookBtn.y += 100; 
			TweenMax.to(this.facebookBtn, 1, {alpha:1, y:this.facebookBtn.y-100, ease:Elastic.easeOut, delay:0.6 });
			this.logo.y = -800;
			TweenMax.to(this.logo, 1.5, {alpha: 1, y:this.logo.y+800, ease:Back.easeOut, delay:0.5 });
			TweenMax.to(this.titulo, 1, {ease:Strong.easeInOut, alpha: 1, delay:0 });
			this.sopas.x -= 300;
			TweenMax.to(this.sopas, 0.8, {alpha: 1, x:this.sopas.x+300, ease:Back.easeOut, delay:0 });
			TweenMax.to(this.escena, 2, {delay:0, alpha:1, ease:Strong.easeInOut, onComplete:test});
			TweenMax.to(this.menuLateral, 2, {delay:0, alpha:1, ease:Strong.easeInOut});
			
		}
		
		private function test(){
			
			this.escena["animacion_"+escenarios[1]].gotoAndStop(1);
			this.escena["animacion_"+escenarios[1]].alpha = 1;
			this.escena["animacion_"+escenarios[1]].play();
			this.escena["video_"+escenarios[1]].alpha = 1;
			this.escena["video_"+escenarios[1]].gotoAndPlay(2);
			this.seccionPrevia = 1;
			
		}
		
		private function onClick(event:MouseEvent):void {
			
			TweenMax.to(event.currentTarget, 1, {glowFilter:{color:0xBD2702, alpha:1, blurX:15, blurY:15}});

			for (var i:Number = 0; i < 3; i++) {
				
				if (this.seccionPrevia != event.currentTarget.seccion){
					trace('seccion= '+ event.currentTarget.seccion);
					trace('seccionPrevia= '+ this.seccionPrevia);
					trace('i= '+ i);
					
					//
					
					//primero limpiamos el video en curso
					if (this.seccionPrevia == i){
						trace('sacando el video= '+ this.seccionPrevia);
						//this.escena["video_"+escenarios[this.seccionPrevia]].gotoAndPlay('f_salir');
						//this.escena["video_"+escenarios[this.seccionPrevia]].miPlayer.stop();
						TweenMax.to(this.escena["animacion_"+escenarios[this.seccionPrevia]], 0.5, {alpha:0});
						TweenMax.to(this.escena["video_"+escenarios[this.seccionPrevia]], 0.7, {alpha:0});
					}
	
					
					if (event.currentTarget.seccion == i){
						
						this.escena["animacion_"+escenarios[event.currentTarget.seccion]].gotoAndStop(1);
						this.escena["animacion_"+escenarios[event.currentTarget.seccion]].alpha = 1;
						this.escena["animacion_"+escenarios[event.currentTarget.seccion]].play();
						
						this.escena["video_"+escenarios[event.currentTarget.seccion]].alpha = 1;
						this.escena["video_"+escenarios[event.currentTarget.seccion]].gotoAndPlay(2);
	
					}
				}
							     
			} 
			this.seccionPrevia = event.currentTarget.seccion;

		}
		
		private function onRollover(event:MouseEvent):void {
			TweenMax.to(event.currentTarget, 1, {glowFilter:{color:0xA36F03, alpha:1, blurX:15, blurY:15}});
		}
		
		private function onRollout(event:MouseEvent):void {
			TweenMax.to(event.currentTarget, 1, {glowFilter:{color:0xA36F03, alpha:0, blurX:30, blurY:30}});
		}
		
		private function onClickFace(event:MouseEvent):void {
			//var targetURL:URLRequest = new URLRequest('http://www.facebook.com');
			var targetURL:URLRequest = new URLRequest('http://www.facebook.com/share.php?u=http://www.burbanoapp.cl/lapausa.cl/staging/index2.php');
			
			try {
				navigateToURL(targetURL, '_blank');
			} catch (e:Error) {
				trace("Error de conexión con Facebook");
			}

		}
		
		private function onRolloverFace(event:MouseEvent):void {
			this.facebookBtn.gotoAndPlay('f_over');
		}
		
		private function onRolloutFace(event:MouseEvent):void {
			this.facebookBtn.gotoAndPlay('f_normal');
		}

		private function agregarListeners():void{
			stage.addEventListener(Event.RESIZE, resizeListener);
			
			this.escena.arteBtn.addEventListener(MouseEvent.CLICK, onClick);
			this.escena.balancesBtn.addEventListener(MouseEvent.CLICK, onClick);
			this.escena.practicanteBtn.addEventListener(MouseEvent.CLICK, onClick);
			
			this.escena.arteBtn.addEventListener(MouseEvent.MOUSE_OVER, onRollover);
			this.escena.balancesBtn.addEventListener(MouseEvent.MOUSE_OVER, onRollover);
			this.escena.practicanteBtn.addEventListener(MouseEvent.MOUSE_OVER, onRollover);
			
			this.escena.arteBtn.addEventListener(MouseEvent.MOUSE_OUT, onRollout);
			this.escena.balancesBtn.addEventListener(MouseEvent.MOUSE_OUT, onRollout);
			this.escena.practicanteBtn.addEventListener(MouseEvent.MOUSE_OUT, onRollout);
			
			//activar video Facebook
			this.facebookBtn.Btn.addEventListener(MouseEvent.CLICK, onClickFace);
			this.facebookBtn.Btn.addEventListener(MouseEvent.MOUSE_OVER, onRolloverFace);
			this.facebookBtn.Btn.addEventListener(MouseEvent.MOUSE_OUT, onRolloutFace);
		}
		
		function resizeListener (e:Event):void {
			moverAlEscalar();
		}
		
		function moverAlEscalar(_inicio:Boolean=false):void {
			diferenciaX = (stage.stageWidth - stgIniW)/2;
			diferenciaY = (stage.stageHeight - stgIniH)/2;
			
			//fondo
			this.referencias[0].x = -diferenciaX;
			this.referencias[0].y = -diferenciaY;
			this.referencias[0].width = stage.stageWidth;
			this.referencias[0].height = stage.stageHeight;
			
			for (var i:int=1; i<this.referencias.length; i++){
				
				if (_inicio){//posicionar antes de que se escale, en caso de que parta grande
					
					this.referencias[i].x = this.posX[i] + (this.margenX[i] * diferenciaX);
					this.referencias[i].y = this.posY[i] + (this.margenY[i] * diferenciaY);
					
				} else {
					
					var ef:Object = {
						x: this.posX[i] + (this.margenX[i] * diferenciaX),
						y: this.posY[i] + (this.margenY[i] * diferenciaY),
						delay:0,
						transition: Strong.easeInOut
					}
					TweenMax.to(this.referencias[i], 1, ef);
					
				}
				
			}
		}
		
	}
	
}
