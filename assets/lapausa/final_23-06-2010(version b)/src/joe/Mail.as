﻿package joe{
	
	import flash.display.Sprite;
	
	import flash.utils.ByteArray;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLVariables;
	
	import flash.errors.IOError;
	
	public class Mail extends Sprite{
		
		//public static const CARA_IMAGEN_ENVIADA:String = 'caraImagenEnviada';

		private var creador:String;
		private var mail:String;
		private var mensaje:String;
		private var hash:String;
		private var tipo_video:int;
		
		private var invitados:Array;
		
		public var img:ByteArray;

		public var loader:URLLoader;
		public var info:String;

		public function Mail() {
			this.loader = new URLLoader();
			this.loader.addEventListener(IOErrorEvent.IO_ERROR, ioerror);
			this.loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, secerror);
			
			this.invitados = new Array();
		}
		
		public function agregarInvitado(_mail:String,_nombre:String=null) {
			this.invitados.push({mail:_mail,nombre:_nombre});
		}
		
		public function setCreador(creador:String,mail:String,mensaje:String,hash:String,tipo_video:int):void {
			this.creador = creador;
			this.mail = mail;
			this.mensaje = mensaje;
			this.hash = hash
			this.tipo_video = tipo_video;
		}
		
		private function removeListeners():void{
			this.loader.removeEventListener(Event.COMPLETE, completeEnviarMailCreador);
			this.loader.removeEventListener(Event.COMPLETE, completeEnviarMailInvitado);
		}
		
		public function commit():void{
			this.enviarMailCreador();
		}

		public function enviarMailCreador():void {
			this.removeListeners();
			this.loader.addEventListener(Event.COMPLETE, completeEnviarMailCreador);
			
			var vars:URLVariables = new URLVariables();
			
			vars.creador = this.creador;
			vars.mail = this.mail;
			vars.mensaje = this.mensaje;
			vars.hash = this.hash;
			vars.tipo_video = this.tipo_video;
			
			trace('vars creador:');
			trace(vars);
			
			this.enviar(Video.RUTA+'enviarMailCreador',vars);
		}
		
		private function completeEnviarMailCreador(event:Event){		
			
			var vars:URLVariables = new URLVariables(event.target.data);
			this.info=vars.toString();
			trace(this.info);
			if(!vars.error == 1){
				this.info+='\nmail a creador enviado... enviando invitaciones';
				trace(this.info);
				
				this.enviarMailInvitados();
			}
			
			//disparando evento
			//this.dispatchEvent(new Event(CARA_IMAGEN_ENVIADA,true));
		}
		
		private function enviarMailInvitados():void{
			for each (var p:Object in this.invitados){
				//trace(p.mail+' '+p.nombre); //OK
				enviarMailInvitado(p);
			}
		}
		
		public function enviarMailInvitado(p:Object):void {
			this.removeListeners();
			this.loader.addEventListener(Event.COMPLETE, completeEnviarMailInvitado);
			
			var vars:URLVariables = new URLVariables();
			
			vars.creador = this.creador;
			vars.mensaje = this.mensaje;
			vars.hash = this.hash;
			vars.tipo_video = this.tipo_video;
			
			vars.mail = p.mail;
			vars.nombre = p.nombre;
			
			trace('vars invitado:');
			trace(vars);
			
			this.enviar(Video.RUTA+'enviarMailInvitado',vars);
		}
		
		private function completeEnviarMailInvitado(event:Event){		
			
			var vars:URLVariables = new URLVariables(event.target.data);
			this.info=vars.toString();
			trace(this.info);
			if(!vars.error == 1){
				this.info+='\nmail a invitado enviado!';
				trace(this.info);
			}
			
			//disparando evento
			//this.dispatchEvent(new Event(CARA_IMAGEN_ENVIADA,true));
		}
		
		private function enviar(url:String,dato:Object):void {
			//trace('enviando cara');
			
			this.info='enviando peticion';
			
			var req:URLRequest=new URLRequest(url);
			req.method=URLRequestMethod.POST;
			req.data=dato;

			try {
				this.loader.load(req);
			} catch (error:IOError) {
				this.info="error: "+error.message;
			} catch (error:Error) {
				this.info="error: "+error.message;
			}
		}
		
		private function ioerror(event:IOErrorEvent) {
			this.info=event.text;
			
			//disparando evento
			//trace('enviando error envio');
			//this.dispatchEvent(new Event(ERROR_ENVIO,true));
		}
		
		private function secerror(event:SecurityError) {
			this.info=event.message;
		}
	}
}
