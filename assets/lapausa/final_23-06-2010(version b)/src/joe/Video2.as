﻿package joe{

	import flash.display.Sprite;

	import flash.utils.ByteArray;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;

	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLVariables;

	import flash.errors.IOError;

	public class Video extends Sprite {
		
		public static const VIDEO_SUBIDO:String = 'videoSubido';

		private var id:int;
		private var tipo_video:int;
		private var creador:String;
		private var mail:String;
		private var mensaje:String;
		private var personas:Array = new Array();
		
		public var hash:String;

		public var loader:URLLoader;

		public static const RUTA:String='/lapausa_final/videos/';
		public var info:String;

		public function Video(tipo:int) {
			this.loader = new URLLoader();
			this.loader.addEventListener(IOErrorEvent.IO_ERROR, ioerror);
			this.loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, secerror);
			
			this.tipo_video = tipo;
		}

		public function agregarPersona(img_cara:ByteArray,img_boca:ByteArray,nombre:String,slot:int) {
			var p:Persona=new Persona(img_cara,img_boca,nombre,slot);
			this.personas[slot]=p;
		}

		public function setCreador(creador:String,mail:String,mensaje:String):void {
			this.creador = creador;
			this.mail = mail;
			this.mensaje = mensaje;
			
			trace('Video.mail = '+this.mail);
		}
		
		public function commit():void{
			//crear video en BD
			this.enviarVideo();
		}

		private function removeListeners():void {
		}
		
		public function enviarVideo():void {
			this.removeListeners();
			this.loader.addEventListener(Event.COMPLETE, completeVideo);

			var vars:URLVariables = new URLVariables();
			
			vars.tipo_video = this.tipo_video;
			vars.creador = this.creador;
			vars.mail = this.mail;
			vars.mensaje = this.mensaje;
			
			this.enviar(RUTA+'setVideo',vars);
		}

		function completeVideo(event:Event) {
			var vars:URLVariables=new URLVariables(event.target.data);
			this.info=vars.toString();
			trace(this.info); //OK
			if (! vars.error==1) {
				this.info+='\nvideo creado... enviando personas';
				this.id = vars.video_id;
				this.hash = vars.hash;
				
				//creando personas
				this.enviarPersonas();
				
				//disparar evento
				this.dispatchEvent(new Event(VIDEO_SUBIDO,true));
			}

			//disparando evento
			//this.dispatchEvent(new Event(VIDEO_ENVIADA,true));
		}
		
		//por privatizar
		public function enviarPersonas():void{
			for each (var p:Persona in this.personas){
				trace(p);
				trace(p.cara+' '+p.boca+''+p.nombre)
				p.commit(this.id);
			}
		}

		private function enviar(url:String,dato:Object):void {
			//trace('enviando cara');

			this.info='enviando video: '+url;
			trace(this.info);

			var req:URLRequest=new URLRequest(url);
			req.method=URLRequestMethod.POST;
			req.data=dato;

			try {
				this.loader.load(req);
			} catch (error:IOError) {
				this.info="error: "+error.message;
			} catch (error:Error) {
				this.info="error: "+error.message;
			}
		}

		private function ioerror(event:IOErrorEvent) {
			this.info=event.text;

			//disparando evento
			//trace('enviando error envio');
			//this.dispatchEvent(new Event(ERROR_ENVIO,true));
		}

		private function secerror(event:SecurityError) {
			this.info=event.message;
		}
	}
}