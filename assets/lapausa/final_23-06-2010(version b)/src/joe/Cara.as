﻿package joe{
	
	import flash.display.Sprite;
	
	import flash.utils.ByteArray;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLVariables;
	
	import flash.errors.IOError;
	
	public class Cara extends Sprite{
		
		//public static const CARA_IMAGEN_ENVIADA:String = 'caraImagenEnviada';

		public var id:int;
		public var url:String;
		public var persona_id:int;
		
		public var img:ByteArray;

		public var loader:URLLoader;
		public var info:String;

		public function Cara(img:ByteArray) {
			this.loader = new URLLoader();
			this.loader.addEventListener(IOErrorEvent.IO_ERROR, ioerror);
			this.loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, secerror);
			
			this.img = img;
		}
		
		private function removeListeners():void{
			this.loader.removeEventListener(Event.COMPLETE, completeCara);
		}
		
		public function commit(id:int):void{
			this.persona_id = id;
			this.enviarCara();
		}

		public function enviarCara():void {
			this.removeListeners();
			this.loader.addEventListener(Event.COMPLETE, completeCara);
			this.enviar(Video.RUTA+'setImagenCara',this.img);
		}
		
		private function completeCara(event:Event){		
			
			var vars:URLVariables = new URLVariables(event.target.data);
			this.info=vars.toString();
			trace(this.info);
			if(!vars.error == 1){
				this.info+='\ncara creada... enviando persona_id';
				this.id = vars.cara_id;
				
				this.actualizarPersonaID();
			}
			
			//disparando evento
			//this.dispatchEvent(new Event(CARA_IMAGEN_ENVIADA,true));
		}
		
		private function actualizarPersonaID():void{
			this.removeListeners();
			this.loader.addEventListener(Event.COMPLETE, completePersonaId);
			
			var vars:URLVariables = new URLVariables();
			
			vars.id = this.id;
			vars.persona_id = this.persona_id;
			
			this.enviar(Video.RUTA+'setCaraPersonaId',vars);
		}
		
		private function completePersonaId(event:Event){		
			
			var vars:URLVariables = new URLVariables(event.target.data);
			this.info=vars.toString();
			
			if(!vars.error == 1){
				this.info='cara actualizada!';
			}
			
			trace(this.info);
			
			//disparando evento
			//this.dispatchEvent(new Event(CARA_IMAGEN_ENVIADA,true));
		}
		
		private function enviar(url:String,dato:Object):void {
			//trace('enviando cara');
			
			this.info='enviando peticion';
			
			var req:URLRequest=new URLRequest(url);
			req.method=URLRequestMethod.POST;
			req.data=dato;

			try {
				this.loader.load(req);
			} catch (error:IOError) {
				this.info="error: "+error.message;
			} catch (error:Error) {
				this.info="error: "+error.message;
			}
		}
		
		private function ioerror(event:IOErrorEvent) {
			this.info=event.text;
			
			//disparando evento
			//trace('enviando error envio');
			//this.dispatchEvent(new Event(ERROR_ENVIO,true));
		}
		
		private function secerror(event:SecurityError) {
			this.info=event.message;
		}
	}
}